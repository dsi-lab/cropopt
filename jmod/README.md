# NSGA optimization of crop planting using DSSAT

## Java UNSGA3 code

### Dependencies
* Java 8

### Run parameters

Make a json file to configure your run. Use the files in the `./config` folder for examples

### Running crop optimization with U-NSGA3
1. Clone project and `cd` into project root
2. To build the project,  run `./gradlew fatJar` 
3. To run the project, run `./run /path/to/config/config.json` 


## Matlab NSGAII code

### Loading jars 
To compile the class:
`javac -classpath dssat-tools-1.0-SNAPSHOT.jar Objective.java`

To import java into main.m
`C:\Users\kroppian\Projects\dssatObjectives`
`C:\Users\kroppian\Projects\dssatObjectives\dssat-tools-1.0-SNAPSHOT.jar`

