
package org.dsi.dssatnsgaconfig;
import org.json.JSONException;
import org.junit.Test;

import java.util.Calendar;
import java.util.GregorianCalendar;
import org.dsi.dssat4j.beans.ResourceApp;

public class ApplicationPeriodTest {

    private final ResourceApp.Method METHOD = ResourceApp.Method.IR004;

    @Test
    public void testCorrectConstructor() {
        ResourceApp app = new ResourceApp(1982, 4, 28, ResourceApp.Substance.WATER, METHOD, 0,4);
        GregorianCalendar startDate = new GregorianCalendar(1982, Calendar.APRIL, 21);
        GregorianCalendar endDate = new GregorianCalendar(1982, Calendar.APRIL, 27);

        new ApplicationPeriod(app.getSubstance(), METHOD, startDate, endDate, 50, 100, 120, 200, 0, 3);
        assert (true);
    }

    @Test
    public void testDifferentYears() {
        ResourceApp app = new ResourceApp(1982, 4, 28, ResourceApp.Substance.WATER, METHOD, 0, 4);
        try {
            GregorianCalendar startDate = new GregorianCalendar(1982, Calendar.APRIL, 21);
            GregorianCalendar endDate = new GregorianCalendar(1983, Calendar.APRIL, 27);
            new ApplicationPeriod(app.getSubstance(), METHOD, startDate, endDate, 50, 100, 120, 200, 0, 3);
        }
        catch (JSONException e) {
            System.err.println("Caught JSONException: " + e.getMessage());
        }

    }

    @Test
    public void testStartAfterEndDate() {
        ResourceApp app = new ResourceApp(1982, 4, 28, ResourceApp.Substance.WATER, METHOD, 0, 4);
        try {
            GregorianCalendar startDate = new GregorianCalendar(1982, Calendar.APRIL, 28);
            GregorianCalendar endDate = new GregorianCalendar(1982, Calendar.APRIL, 26);
            new ApplicationPeriod(app.getSubstance(), METHOD, startDate, endDate, 50, 100, 120, 200, 0, 3);
        }
        catch (JSONException e) {
            System.err.println("Caught JSONException: " + e.getMessage());
        }

    }

    @Test
    public void testTimesApplesMax() {
        ResourceApp app = new ResourceApp(1982, 4, 28, ResourceApp.Substance.WATER, METHOD, 0, 4);
        try {
            GregorianCalendar startDate = new GregorianCalendar(1982, Calendar.APRIL, 26);
            GregorianCalendar endDate = new GregorianCalendar(1982, Calendar.APRIL, 28);
            new ApplicationPeriod(app.getSubstance(), METHOD, startDate, endDate, 50, 100, 120, 200, 0, 3);
        }
        catch (JSONException e) {
            System.err.println("Caught JSOException: " + e.getMessage());
        }

    }


    @Test
    public void testAmountVsPeriodSupplied() {
        ResourceApp app = new ResourceApp(1982, 4, 28, ResourceApp.Substance.WATER, METHOD, 0, 4);
        try {
            GregorianCalendar startDate = new GregorianCalendar(1982, Calendar.APRIL, 21);
            GregorianCalendar endDate = new GregorianCalendar(1982, Calendar.APRIL, 27);
            new ApplicationPeriod(app.getSubstance(), METHOD, startDate, endDate, 50, 200, 99, 100, 0, 3);
        }
        catch (JSONException e) {
            System.err.println("Caught JSOException: " + e.getMessage());
        }

    }

    @Test
    public void testNegativeParams() {
        ResourceApp app = new ResourceApp(1982, 4, 28, ResourceApp.Substance.WATER, METHOD, 0, 4);
        try {
            GregorianCalendar startDate = new GregorianCalendar(1982, Calendar.APRIL, 21);
            GregorianCalendar endDate = new GregorianCalendar(1982, Calendar.APRIL, 27);
            new ApplicationPeriod(app.getSubstance(), METHOD, startDate, endDate, -50, 100, 120, 200, 0, 3);
        }
        catch (JSONException e) {
            System.err.println("Caught JSOException: " + e.getMessage());
        }

    }


}
