
package org.dsi.dssatnsga;

import org.dsi.dssat4j.beans.ResourceApp;
import org.dsi.dssatnsgaconfig.ApplicationPeriod;
import org.json.JSONException;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.List;

public class ConstraintTest {

    List<Integer> VARIABLE_LIST_A = new ArrayList<Integer>(Arrays.asList(1,2,3,5,8,13,21));
    List<Integer> VARIABLE_LIST_B = new ArrayList<Integer>(Arrays.asList(1,2,3,5,8,13,21));

    @Test
    public void testConstraintEquals() {
        Constraint a = new Constraint(ResourceApp.Substance.WATER, 0.1, 23.8, this.VARIABLE_LIST_A);
        Constraint b = new Constraint(ResourceApp.Substance.WATER, 0.1, 23.8, this.VARIABLE_LIST_B);
        assert(a.equals(b));

    }

    @Test
    public void testConstraintDoesNotEqual() {
        // Different substance
        Constraint a = new Constraint(ResourceApp.Substance.NITROGEN, 0.1, 23.8, this.VARIABLE_LIST_A);
        Constraint b = new Constraint(ResourceApp.Substance.WATER, 0.1, 23.8, this.VARIABLE_LIST_B);
        assert(!a.equals(b));

        // Different minimum
        a = new Constraint(ResourceApp.Substance.WATER, 0.4, 23.8, this.VARIABLE_LIST_A);
        b = new Constraint(ResourceApp.Substance.WATER, 0.1, 23.8, this.VARIABLE_LIST_B);
        assert(!a.equals(b));

        // Different maximum
        a = new Constraint(ResourceApp.Substance.WATER, 0.1, 23.9, this.VARIABLE_LIST_A);
        b = new Constraint(ResourceApp.Substance.WATER, 0.1, 23.8, this.VARIABLE_LIST_B);
        assert(!a.equals(b));

        // Different list
        List<Integer> modifiedListA = new ArrayList<>(this.VARIABLE_LIST_A);
        modifiedListA.add(4);
        a = new Constraint(ResourceApp.Substance.WATER, 0.1, 23.8, modifiedListA);
        b = new Constraint(ResourceApp.Substance.WATER, 0.1, 23.8, this.VARIABLE_LIST_B);
        assert(!a.equals(b));
    }




}
