
package org.dsi.dssatnsga;

import org.dsi.dssat4j.beans.ResourceApp;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ConstraintsTest {

    List<Integer> VARIABLE_LIST_A = new ArrayList<Integer>(Arrays.asList(1,2,3,5,8,13,21));
    List<Integer> VARIABLE_LIST_B = new ArrayList<Integer>(Arrays.asList(1,2,3,5,8,13,21));

    @Test
    public void testNoDuplicates() {
        Constraint a = new Constraint(ResourceApp.Substance.WATER, 0.1, 23.8, this.VARIABLE_LIST_A);
        Constraint b = new Constraint(ResourceApp.Substance.WATER, 0.1, 23.8, this.VARIABLE_LIST_B);
        assert(a.equals(b));

    }




}
