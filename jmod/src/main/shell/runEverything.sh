#!/bin/bash

tempDir=/tmp/wat-irr

projectRoot=/home/ian/dssat-optimization/
configDir=${projectRoot}/workspace-irr

echo "Generating dates..."
python ${projectRoot}/scripts/appdategen.py 1982 10000 -i 16 --imin 60 --imax 180  > ${tempDir}/dates.txt

echo "Dates generated. Generating config files"
${projectRoot}/scripts/genConfs.sh ${tempDir}/dates.txt  ${projectRoot}/scripts/config_template-irr.txt ${tempDir} ${configDir}

dateTime=$(date +%F-%H%M)
remoteBackupAddress='35.10.57.190'
remoteBackupDirectory="/c/home/kroppian/cropModeling/$dateTime"

status=0
ssh root@$remoteBackupAddress mkdir -p "${remoteBackupDirectory}" 

status=${?}
if [[ ${status} -ne 0 ]]
  then
  echo "Unable to create the output directory on the remote machine."
  echo "${dirCreationResults}"
  exit 1
fi

rm ${tempDir}/dates.txt

cd ${projectRoot}/dssat-nsga

rm -rvf ${tempDir}/run-*

${projectRoot}/scripts/runOptBatch.sh ${configDir}/* ${tempDir} ${remoteBackupDirectory} 

