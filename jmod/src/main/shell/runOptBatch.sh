#!/bin/bash

trap 'errorReport $LINENO' ERR

function errorReport {
  echo "runOptBatch: Error in line ${1}. Exiting" > /dev/stderr
  exit 1 
}

function usage {
  echo "${0} CONFIG1 [CONFIG2 ... CONFIGN] OUTPUTDIR REMOTEBACKUPDIR"
}

if [[ $# -lt 3 ]];then 
  usage
fi

args=( $@ )


((endOfConfigFiles = $# - 2, 1))

((outputDir = $# - 2, 1))

((remoteBackupDirectory = $# - 1, 1))

outputDir=${args[$outputDir]}

remoteBackupDirectory=${args[$remoteBackupDirectory]}
configFiles=( ${args[@]:0:$endOfConfigFiles} )
remoteBackupAddress='35.10.57.190'

for file in ${configFiles[@]}
  do

  echo "Running file [$file]"

  id=$(echo $file | grep -Po '\d{5}.json$' | grep -Po '\d{5}') 

  mkdir -p ${outputDir}/run-${id}

  #echo "mvn exec:java -Dexec.args=\"$file\" # Hello world!" >> ${outputDir}/run-${id}/out.txt
  mvn exec:java -Dexec.args="$file" 2> /dev/stdout > ${outputDir}/run-${id}/out.txt
  #mvn exec:java -Dexec.args="$file" 

  mv ${file} ${outputDir}/run-${id}/

  zip -qr --junk-paths ${outputDir}/run-${id}.zip ${outputDir}/run-${id} 


  if [[ $? -ne 0 ]] 
    then
    echo "Error zipping results" > /dev/stderr
    continue
  fi 

  for try in {1..30}
  do

    status=0
    scp ${outputDir}/run-${id}.zip root@${remoteBackupAddress}:${remoteBackupDirectory} > /dev/null 2> /dev/null || status=${?}

    if [[ $status -eq 0 ]]
      then
      echo "Transfer ${id} successful on try ${try}"
      break
    fi

    sleep $try

  done

  if [[ ${status} -ne 0 ]] 
    then
    echo "scp failed for ${id}. Trying mv"
    status=0
    mv -v ${outputDir}/run-${id}.zip ~/ || status=${?}
  fi

  if [[ $status -eq 0 ]]
  then
    rm -rf ${outputDir}/run-${id}/
    rm -f ${outputDir}/run-${id}.zip
  else
    echo "Unable to copy output ${id} to backups" > /dev/stderr
    continue 
  fi

  echo "batch ${id} completed"

done


  


