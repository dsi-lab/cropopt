#!/bin/bash

trap 'errorReport $LINENO' ERR

function errorReport {
  echo "genConfs: Error in line ${1}. Exiting" > /dev/stderr
  exit 1 
}

function usage {

  echo "Generate Config files"
  echo "Generates a number of nsga-dssat config files with random dates from appdategen"
  echo "Usage: genConfs DATE_FILE HEADER_TEMPLATE DSSATNSGA3_OUT_DIRECTORY CONFIG_OUTPUT_DIRECTORY1 [CONFIG_OUTPUT_DIRECTORY2 .. CONFIG_OUTPUT_DIRECTORYN]"

}

function getAppEntry {
  echo   "    {\"type\": \"${1}\","
  echo   "      \"start-date\":\"${2}\","
  echo   "      \"end-date\":\"${2}\","
  echo   '      "applied-min":"0",'
  echo   '      "applied-max":"100",'
  echo   '      "total-applied-min":"0",'
  echo   '      "total-applied-max":"100",'
  echo   '      "times-applied-min":"1",'
  printf '      "times-applied-max":"1"}'
  
}

function getHeader {
  if [[ ! -f ${1} ]]
    then
    echo "Header file [${1}] not found" > /dev/stderr
    exit 1
  fi 

  if [[ -z ${2} ]]
    then
    echo "No output dir given" > /dev/stderr
    exit 1
  fi 

  cat ${1} | sed "s|OUTPUT_DIRECTORY|${2}|"
  return $? 

}

function getFooter {
  echo '  ]'
  echo '}'
}

if [[ $# -lt 4 ]]
  then 

  usage > /dev/stderr 
  exit 1

fi

dates="$(cat ${1})"

headerFile=${2}

dssatnsga3OutputDir=${3}

IFS=$'\n\r'


# Find out the number of experiments we're generating
numberOfLines=$(echo "$dates" | wc -l)

((numberOfExperiments = $numberOfLines / 3, 1))

# Get the output directories
outputDirs=(${*})
outputDirs=( "${outputDirs[@]:3}" ) 
directoryCount=${#outputDirs[@]}

((runsPerDirectory= ($numberOfExperiments / $directoryCount), 1))

currentFileIndex=0
currentDirectoryIndex=-1

for line in $dates
do


  IFS=$','

  arr=(${line})

  appType=${arr[0]}

  # Check for the beginning of the file
  if [[ ${appType} = 'WATER' ]]
  then

    first=true;

    ((currentDirectoryCheck = ${currentFileIndex} % ${runsPerDirectory}, 1))

    if [[ ${currentDirectoryCheck} -eq 0 ]]
      then
      (( currentDirectoryIndex = ${currentDirectoryIndex} + 1, 1))
      (( currentDirectoryIndex = ${currentDirectoryIndex} % ${directoryCount}, 1))
    fi

    outputFile="${outputDirs[${currentDirectoryIndex}]}/config$(printf "%05d" ${currentFileIndex}).json"

    echo "Creating file ${outputFile}"

    getHeader ${headerFile} "${dssatnsga3OutputDir}/run-$(printf "%05d" ${currentFileIndex})/" > $outputFile

  fi

  dates=${arr[@]:1}

  if [[ ${appType} != 'WATER' && ${appType} != 'NITROGEN' && ${appType} != 'PHOSPHORUS' ]]
    then

    echo "Unknown application type: [${appType}]" > /dev/stderr
    exit 1

  fi

  for appDate in "${arr[@]:1}"
    do

    if [[ ${first} = true ]] 
      then
      first=false;
    elif [[ ${appType} != 'PHOSPHORUS' ]]
      then
      echo "," >> ${outputFile}
    fi

    getAppEntry ${appType} ${appDate} >> ${outputFile}

  done

  echo >> ${outputFile}

  # Check for the end of the file
  if [[ ${appType} = 'PHOSPHORUS' ]]
  then
    getFooter >> ${outputFile}
    (( currentFileIndex = ${currentFileIndex} + 1, 1))
  fi


done

echo "Config files successfully generated"

exit 0 

