#!/bin/bash 

function usage {
  echo ${1} > /dev/stderr
  echo "Usage: ${0} TEMPLATE OUTDIR STARTYEAR ENDYEAR " > /dev/stderr
  exit 1
}

template=${1}
outDir=${2}
startYear=${3}
endYear=${4}

yrreg='^\d{4}$'

if [[ -z $(echo ${startYear} | grep -Po ${yrreg}) || -z $(echo ${endYear} | grep -Po ${yrreg}) ]]; then
  usage "invalid year"
fi

if [[ ${startYear} -gt ${endYear} ]];then
  usage "invalid year range"
fi

if [[ ! -d ${outDir} || ! -f ${template}  ]];then
  usage "Invalid output dir or temp file"
fi

expgenPath=$(dirname $0)/../python/tools/configgen/expgen.py 

for year in $(seq ${startYear} ${endYear}); do 
  #yy=$( echo $year | grep -Po '\d\d$')
  output=$(python3 ${expgenPath} ${template} ${year})
  if [[ -z $(echo "${output}" | grep -iPo 'usage') ]]
  then
    echo "${output}"  > ${outDir}/$(echo $template | sed "s/\./$year./")
  else
    echo "probz: ${output}"
    exit 1
  fi

done

