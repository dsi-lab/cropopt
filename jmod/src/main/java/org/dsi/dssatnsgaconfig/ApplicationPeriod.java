package org.dsi.dssatnsgaconfig;

import org.dsi.dssat4j.beans.ResourceApp;
import org.json.JSONException;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class ApplicationPeriod {
    private ResourceApp.Substance type;

    private GregorianCalendar startDate;
    private GregorianCalendar endDate;
    private double amountSuppliedMin;
    private double amountSuppliedMax;
    private double periodAmountSuppliedMin;
    private double periodAmountSuppliedMax;
    private int depth;
    private int timesAppliedMin;
    private int timesAppliedMax;
    private ResourceApp.Method method;

    public ApplicationPeriod(ResourceApp.Substance type, ResourceApp.Method method, GregorianCalendar startDate,
                             GregorianCalendar endDate, double amountSuppliedMin,
                             double amountSuppliedMax, double periodAmountSuppliedMin,
                             double periodAmountSuppliedMax, int timesAppliedMin,
                             int timesAppliedMax) throws JSONException {

        // make sure all the years are the same (startDate, endDate)
        if (startDate.get(Calendar.YEAR) != endDate.get(Calendar.YEAR))
            throw new JSONException("Years do not match");


        // make sure the ranges are valid (startDate is before endDate)
        if (startDate.after(endDate))
            throw new JSONException("startDate cannot be after endDate");


        // Make sure that the timesAppliedMax isn't greater than the number of dates between startDate and endDate
        long differenceInMillis = (endDate.getTimeInMillis() - startDate.getTimeInMillis());
        long differenceInDays = (differenceInMillis/(86400000)) + 1; // plus one because we include the first and last day
        if(timesAppliedMax > differenceInDays)
            throw new JSONException("timesAppliedMax should be less than number of dates between startDate and endDate");


        // amountSuppliedMax cannot be above periodAmountSuppliedMax
        if(amountSuppliedMax > periodAmountSuppliedMax)
            throw new JSONException("amountSuppliedMax cannot be above periodAmountSuppliedMax");


        // amountSuppliedMin, amountSuppliedMax, periodAmountSuppliedMax, periodAmountSuppliedMin, timesAppliedMax, timesAppliedMin  all should be non-negative
        if(amountSuppliedMin < 0 || amountSuppliedMax < 0 || periodAmountSuppliedMax < 0 || periodAmountSuppliedMin < 0 || timesAppliedMax < 0 || timesAppliedMin < 0)
            throw new JSONException("amountSuppliedMin ["+ amountSuppliedMin +"], amountSuppliedMax ["+ amountSuppliedMax +"], periodAmountSuppliedMax ["+ periodAmountSuppliedMax +"], periodAmountSuppliedMin ["+ periodAmountSuppliedMin +"], timesAppliedMax ["+ timesAppliedMax +"], timesAppliedMin ["+ timesAppliedMin +"]  all should be non-negative");


        this.type = type;
        this.method = method;
        this.startDate = startDate;
        this.endDate = endDate;
        this.amountSuppliedMin = amountSuppliedMin;
        this.amountSuppliedMax = amountSuppliedMax;
        this.periodAmountSuppliedMax = periodAmountSuppliedMax;
        this.periodAmountSuppliedMin = periodAmountSuppliedMin;
        this.timesAppliedMax = timesAppliedMax;
        this.timesAppliedMin = timesAppliedMin;

    }


    public ApplicationPeriod(ResourceApp.Substance type, ResourceApp.Method method, GregorianCalendar startDate,
                             GregorianCalendar endDate, double amountSuppliedMin,
                             double amountSuppliedMax, double periodAmountSuppliedMin,
                             double periodAmountSuppliedMax, int timesAppliedMin,
                             int timesAppliedMax, int depth) throws JSONException {

        this(type, method, startDate, endDate,  amountSuppliedMin, amountSuppliedMax,  periodAmountSuppliedMin,
                periodAmountSuppliedMax,  timesAppliedMin, timesAppliedMax);

        this.depth = depth;

    }

    public ResourceApp.Substance getType(){
        return this.type;
    }

    public GregorianCalendar getEndDate() {
        return this.endDate;
    }

    public GregorianCalendar getStartDate() {
        return this.startDate;
    }

    public double getAppliedMin() {
        return this.amountSuppliedMin;
    }

    public double getAppliedMax() {
        return this.amountSuppliedMax;
    }

    public double getTotalAppliedMin(){ return this.periodAmountSuppliedMin; }

    public double getTotalAppliedMax(){ return this.periodAmountSuppliedMax; }

    public int getTimesAppliedMin(){
        return this.timesAppliedMin;
    }

    public int getTimesAppliedMax(){
        return this.timesAppliedMax;
    }

    public int getDepth(){ return this.depth; }

    public ResourceApp.Method getMethod(){ return this.method; }

}
