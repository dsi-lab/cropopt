package org.dsi.dssatnsgaconfig;

import net.sourceforge.jeval.function.math.Exp;
import org.apache.commons.io.IOUtils;
import org.dsi.dssat4j.beans.ResourceApp;
import org.dsi.dssat4j.exceptions.FileParseException;
import org.dsi.dssat4j.exceptions.IllegalStringFormatException;
import org.dsi.dssat4j.IO.Experiment;
import org.dsi.dssat4j.tools.DssatTools;
import org.dsi.dssatnsga.Constraint;
import org.dsi.dssatnsga.Constraints;
import org.dsi.dssatnsga.Objective;
import org.dsi.dssatnsgaconfig.ApplicationDate;
import org.dsi.dssatnsgaconfig.ApplicationPeriod;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Array;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import java.util.Random;


public class DssatNsga3RunConfig {

    private final String[] AVAILABLE_OBJECTIVES = {"YIELD", "LEACHING_NITROGEN", "TOTAL_WATER",
            "TOTAL_PHOSPHORUS", "TOTAL_NITROGEN", "GROSS_MARGIN", "STOCHASTIC_YIELD_MEAN", "STOCHASTIC_YIELD_VARIANCE" };

    private List<ApplicationPeriod> applicationPeriods;
    private List<ApplicationDate> applicationDates;
    private DssatTools tools;
    private String runDirectory;
    private int threads;
    private int generations;
    private String outputDir;
    private List<Objective> objectives;
    private Constraints constraints;
    private JSONArray constraintJson;
    private String executablePath;
    private String institutionCode;
    private String siteCode;
    private int year;
    private int experimentNumber;
    private String profilePath;
    private String experimentFilePath;
    private int runs;
    private int population;
    private int logLevel;
    private Random randNumGenerator; 

    public String getExecutablePath(){
        return this.executablePath;
    }

    public String getExperimentFilePath(){ return this.experimentFilePath; }

    public String getProfilePath(){ return this.profilePath; }

    public String getInstitutionCode(){
        return this.institutionCode;
    }

    public String getSiteCode(){
        return this.siteCode;            
    }

    public int getYear(){
        return this.year;
    }

    public int getExperimentNumber(){
        return this.experimentNumber;
    }

    public String getRunDirectory(){
        return this.runDirectory;
    }

    public int getThreads(){
        return this.threads;
    }

    public int getGenerations(){
        return this.generations; 
    }

    public String getOutputDir(){
        return this.outputDir;
    }

    public int getLogLevel(){ return this.logLevel; }

    public int getRuns(){ return this.runs; }

    public int getPopulation(){ return this.population; }

    public void regenerateDates() {
        this.applicationDates = generateDates(this.applicationPeriods);
        try{
            this.constraints = generateConstraints(this.applicationPeriods, this.applicationDates, this.constraintJson);
        }catch(IOException e){ /* This shouldn't happen because we've already parsed the JSON. Sorry, I know it's sloppy. */ }
    }

    public List<Objective> getObjectives() {
        return this.objectives;
    }

    public List<ApplicationPeriod> getApplicationPeriods(){
        return this.applicationPeriods;
    }

    public List<ApplicationDate> getApplicationDates(){
        return this.applicationDates;
    }

    public Constraints getConstraints(){ return this.constraints; }

    public DssatNsga3RunConfig(List<ApplicationPeriod> periods){
        this.applicationPeriods = periods;
    }

    /* Main parsing routine  */
    public DssatNsga3RunConfig(String configPath) throws JSONException, IOException, FileParseException {

        this.randNumGenerator = new Random(25);

        this.tools = new DssatTools();
        String url = "";
        try{
            url = IOUtils.toString(new URL("file","localhost", configPath));
        }catch(MalformedURLException e){
            System.err.println(e.getMessage());
            System.err.println("File [" + configPath + "] not found." );
            System.exit(1);
        }catch(IOException e){
            System.err.println("Error reading [" + configPath + "]" );
            System.exit(1);
        }

        JSONObject root = new JSONObject(url);

        // Get objectives
        JSONArray jsonObjectivesObject = null;
        this.objectives = new ArrayList<>();

        jsonObjectivesObject = (JSONArray) root.get("objectives");
        // Iterate through the array and parse the objective data
        for(int i = 0; i < jsonObjectivesObject.length(); i++){

            // Get the objective object
            JSONObject objectiveObj = (JSONObject) jsonObjectivesObject.get(i);

            // Get the objective type
            String currentObjective = (String)objectiveObj.get("type");
            if(!Arrays.asList(AVAILABLE_OBJECTIVES).contains(currentObjective))
                throw new JSONException("Objective " + currentObjective + "not supported.") ;
            this.objectives.add(new Objective(currentObjective));

            // Get the objective scaling if (if it exists)
            JSONObject scalingObj = null;
            try{
                scalingObj = (JSONObject) objectiveObj.get("scaling");
            }catch(JSONException e){
                /* Do nothing, since this is an optional object*/ }
            if(scalingObj != null) {
                this.objectives.get(i).setScalingMin((double)((Integer) scalingObj.get("min")).intValue());
                this.objectives.get(i).setScalingMax((double)((Integer) scalingObj.get("max")).intValue());
            }

        }

        // Get the application periods
        JSONArray applicationPeriods = null;
        try{
            applicationPeriods = (JSONArray) root.get("application-periods");
            this.applicationPeriods = parseApplicationPeriods(applicationPeriods);
        }catch(JSONException e){
            throw new JSONException("Unable to find the \"application-periods\" attribute in the JSON root.\n" + e.getMessage());
        }

        this.applicationDates = generateDates(this.applicationPeriods);

        this.constraintJson = (JSONArray) root.get("constraint-periods");

        this.constraints = generateConstraints(this.applicationPeriods, this.applicationDates, this.constraintJson);

        // Get DSSAT info
        JSONObject dssatConfig = null;
        try{
            dssatConfig = (JSONObject) root.get("dssatConf");
            this.runDirectory = (String) dssatConfig.get("runDirectory");
            this.institutionCode = (String) dssatConfig.get("institutionCode");
            this.siteCode = (String) dssatConfig.get("siteCode");
            this.year = (int) dssatConfig.get("year");
            this.experimentNumber = (int) dssatConfig.get("experimentNumber");
            this.executablePath = (String) dssatConfig.get("executablePath");
            this.profilePath = (String) dssatConfig.get("profilePath");
            this.experimentFilePath = (String) dssatConfig.get("experimentFilePath");

        }catch(JSONException e){ throw new JSONException("Unable to find the \"dssatConf\" attribute in the JSON root, or an attirbute within.\n" + e.getMessage()); 
        }catch(ClassCastException e){ throw new JSONException("Invalid type given within the \"dssatConf\" attribute\n" + e.getMessage()); }

        // Get NSGA info
        try{
            dssatConfig = (JSONObject) root.get("unsgaIIIConf");
            this.threads = (Integer) dssatConfig.get("threads");
            this.outputDir = (String) dssatConfig.get("outputDir");
            this.generations = (Integer) dssatConfig.get("generations");
            this.runs = (Integer)  dssatConfig.get("runs");
            this.population = (Integer) dssatConfig.get("population");
            this.logLevel = (Integer) dssatConfig.get("logLevel");
        }catch(JSONException e){ throw new JSONException("Unable to find the \"unsgaIIIConf\" attribute in the JSON root, or an attirbute within."); }

    }

    private static Constraints generateConstraints(List<ApplicationPeriod> periods,
                                                   List<ApplicationDate> dates,
                                                   JSONArray rawConstraintJson) throws IOException {

        Constraints result = new Constraints();

        for(ApplicationPeriod period : periods){
            Constraint newPeriodConstraint = new Constraint(period.getType(),period.getTotalAppliedMin(), period.getTotalAppliedMax());
            int dateInd = 0;
            for(ApplicationDate date : dates){
                if(date.getPeriod().equals(period)){
                    newPeriodConstraint.addVariable(dateInd);
                }
                dateInd++;
            }
            result.addConstraint(newPeriodConstraint);
        }


        // Make individual constraints for each of the dates
        int dateInd = 0;
        for(ApplicationDate date : dates){
            result.addConstraintPeriod(
                    date.getType(),date.getApplicationMin(),
                    date.getApplicationMax(),new ArrayList<Integer>(Arrays.asList(dateInd)));
            dateInd++;
        }

        // Get the constraint periods
        try{
            result.append(parseConstraintPeriods(rawConstraintJson,dates));
        }catch(JSONException e){
            throw new JSONException("Unable to find the \"constraint-periods\" attribute in the JSON root, or an attirbute within.\n" + e.getMessage());
        }
        return result;

    }

    private List<ApplicationDate> generateDates(List<ApplicationPeriod> periods){

        int[] constraintVars;
        Integer dateIndices[];
        List<ApplicationDate> result = new ArrayList<ApplicationDate>();

        int dateInd = 0;
        for(ApplicationPeriod period : periods){
            int startDate = period.getStartDate().get(Calendar.DAY_OF_YEAR);
            int endDate = period.getEndDate().get(Calendar.DAY_OF_YEAR);

            int timesAppliedMin = period.getTimesAppliedMin();
            int timesAppliedMax = period.getTimesAppliedMax();

            // Randomly generate the number of applications allowed in the period
            int numberOfApplications = this.randNumGenerator.nextInt((timesAppliedMax + 1) - timesAppliedMin) + timesAppliedMin;

            dateIndices = new Integer[numberOfApplications];

            constraintVars = new int[numberOfApplications];

            int proposedDate;
            for(int i = 0; i< numberOfApplications; i++) {

                // Record which variables are in this application
                constraintVars[i] = result.size();
                if (endDate != startDate){
                    do {
                        proposedDate = this.randNumGenerator.nextInt(endDate - startDate);
                    } while (Arrays.asList(dateIndices).contains(proposedDate));
                }else{
                    proposedDate = 0;
                }
                dateIndices[i] = proposedDate;

                GregorianCalendar newDate =  (GregorianCalendar) period.getStartDate().clone();
                newDate.set(Calendar.DAY_OF_YEAR, newDate.get(Calendar.DAY_OF_YEAR) + proposedDate);

                if(period.getType().toString().equals("NITROGEN"))
                    result.add(new ApplicationDate(period.getType(), period.getMethod(), newDate, period.getAppliedMin(), period.getAppliedMax(), period.getDepth()));
                else
                    result.add(new ApplicationDate(period.getType(), period.getMethod(), newDate, period.getAppliedMin(), period.getAppliedMax() ));


                result.get(dateInd).setPeriod(period);
                dateInd++;
            }

        }

        return  result;

    }

    private static String[] getValues(JSONObject object, String[] fields) throws JSONException {
        String[] result = new String[fields.length];
        int j = 0;
        for(String field : fields){

            try{
                if(object.get(field) instanceof Integer){
                    result[j] = ((Integer) object.get(field)).toString();
                }else if(object.get(field) instanceof String) {
                    result[j] = (String) object.get(field);
                }else{
                    throw new JSONException("Expected application field " + field + " in application " +
                            " to be either a string or an integer ");
                }
            }catch(JSONException e){
                throw new JSONException("Could not find the \"" + field + "\" field within application");
            }
            result[j] =  result[j].toLowerCase();
            j++;
        }
        return result;

    }

    private static ResourceApp.Substance getType(String type) throws JSONException {

        ResourceApp.Substance result;
        switch(type){
            case "water":
                result = ResourceApp.Substance.WATER;
                break;
            case "nitrogen":
                result = ResourceApp.Substance.NITROGEN;
                break;
            case "phosphorus":
                result = ResourceApp.Substance.PHOSPHORUS;
                break;
            case "potassium":
                result = ResourceApp.Substance.POTASSIUM;
                break;
            default:
                throw new JSONException("Invalid application type [" + type + "]");
        }

        return result;

    }

    private static GregorianCalendar parseDate(String date) throws JSONException {

        String[] dateArr = date.split("-");
        GregorianCalendar result;
        if(dateArr.length == 3) {
            int year = Integer.parseInt(dateArr[0]);
            int month = Integer.parseInt(dateArr[1]);
            int day = Integer.parseInt(dateArr[2]);
            if(year < 1900 || year > 2100 || month < 1 || month > 12 || day < 1 || day > 31)
                throw new JSONException("Invalid date " + date);
            result = DssatTools.convert2Greg( year, month, day);
          
        }else if(date.matches("\\d{5}")){
            try {
                result = DssatTools.convert2Greg(date);
            }catch(IllegalStringFormatException e){
                throw new JSONException("Invalid date " + date);
            }
        }else 
            throw new JSONException("Invalid date " + date);



        return result;

    }

    private static Constraints parseConstraintPeriods(JSONArray constraints, List<ApplicationDate> applicationDates) throws JSONException, IOException {

        String[] constraintFields =  {"type", "start-date", "end-date", "applied-min" , "applied-max"};

        Constraints result = new Constraints();

        String[] values;
        ResourceApp.Substance type;

        for(int i = 0; i < constraints.length(); i++){

            values = getValues((JSONObject) constraints.get(i), constraintFields);
            type = getType(values[0]);
            GregorianCalendar startDate = parseDate(values[1]);
            GregorianCalendar endDate = parseDate(values[2]);

            double appliedMin = Double.parseDouble(values[3]);
            double appliedMax = Double.parseDouble(values[4]);;

            List<Integer> variablesInConstraint = new ArrayList<Integer>();

            // loop through application dates and find those that fall within constraint
            int j = 0;
            for(ApplicationDate date : applicationDates){
                if(date.getDate().compareTo(startDate) > 0 && date.getDate().compareTo(endDate) <= 0)
                    variablesInConstraint.add(j);
                j++;
            }

            if(variablesInConstraint.size() != 0)
                result.addConstraintPeriod(type,appliedMin, appliedMax, variablesInConstraint);

        }

        return result;
    }


    private List<ApplicationPeriod> parseApplicationPeriods(JSONArray applications) throws JSONException, IOException {

        String[] applicationFields =  {"type", "start-date", "end-date",
                "applied-min" , "applied-max" , "total-applied-min",
                "total-applied-max", "times-applied-max" , "times-applied-min", "method" };

        String[] fertFields = {"depth"};

        List<ApplicationPeriod> applicationPeriods = new ArrayList<ApplicationPeriod>(applications.length());

        for(int i = 0; i < applications.length(); i++){

            ResourceApp.Substance type;

            String[] values = getValues((JSONObject) applications.get(i), applicationFields);

            // Application type
            type = getType(values[0]);

            // Start date
            GregorianCalendar startDate = parseDate(values[1]);

            // End date
            GregorianCalendar endDate = parseDate(values[2]);

            double appliedMin = Double.parseDouble(values[3]);
            double appliedMax = Double.parseDouble(values[4]);;
            double totalAppliedMin = Double.parseDouble(values[5]);;
            double totalAppliedMax = Double.parseDouble(values[6]);;
            int timesAppliedMax = Integer.parseInt(values[7]);;
            int timesAppliedMin = Integer.parseInt(values[8]);;
            ResourceApp.Method method = ResourceApp.Method.valueOf(values[9].toUpperCase());

            // Fertilizer specific fields
            if(type.toString().equals("NITROGEN")){
                String[] fertValues = getValues((JSONObject) applications.get(i), fertFields);
                int  depth = Integer.parseInt(fertValues[0]);

                applicationPeriods.add(new ApplicationPeriod(type, method, startDate, endDate,
                        appliedMin, appliedMax, totalAppliedMin,
                        totalAppliedMax, timesAppliedMin, timesAppliedMax, depth));

            }else{
                // Generic fields
                applicationPeriods.add(new ApplicationPeriod(type, method, startDate, endDate,
                        appliedMin, appliedMax, totalAppliedMin,
                        totalAppliedMax, timesAppliedMin, timesAppliedMax));

            }





        }

        return applicationPeriods;
    }


}
