package org.dsi.dssatnsgaconfig;

import org.dsi.dssat4j.beans.ResourceApp;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class ApplicationDate {

    private ResourceApp.Substance type;
    private GregorianCalendar date;
    private double applicationMin;
    private double applicationMax;
    private ApplicationPeriod period;
    private int depth;
    private ResourceApp.Method method;

    public ApplicationDate(ResourceApp.Substance type,
                           ResourceApp.Method method,
                           GregorianCalendar date,
                           double min,
                           double max){
        this.type = type;
        this.method = method;
        this.date = date;
        this.applicationMin = min;
        this.applicationMax = max;
    }

    public ApplicationDate(ResourceApp.Substance type,
                           ResourceApp.Method method,
                           GregorianCalendar date,
                           double min,
                           double max, int depth){
        this.type = type;
        this.method = method;
        this.date = date;
        this.applicationMin = min;
        this.applicationMax = max;
        this.depth = depth;
    }


    public ResourceApp.Substance getType(){
        return this.type;
    }

    public GregorianCalendar getDate() {
        return this.date;
    }

    public double getApplicationMin(){ return this.applicationMin; }

    public double getApplicationMax(){ return this.applicationMax; }

    public int getApplicationDepth(){ return this.depth; }

    public ResourceApp.Method getMethod(){ return this.method; }


    public ApplicationPeriod getPeriod() {
        return period;
    }

    public void setPeriod(ApplicationPeriod period){
        this.period = period;
    }

    public String toString(){
        return this.date.get(Calendar.YEAR) + "-"
                + (this.date.get(Calendar.MONTH) + 1) + "-"
                + this.date.get(Calendar.DAY_OF_MONTH) + " "
                + this.type;
    }

}
