package org.dsi.dssatnsgaconfig;

import emo.OptimizationProblem;
import emo.RealVariableSpecs;
import emo.Variable;
import org.dsi.dssatnsga.CropEvaluator;
import org.dsi.dssatnsga.Objective;

import java.util.ArrayList;
import java.util.List;

public class Nsga3ConfigGenerators {

    public static Variable[] generateVariable(DssatNsga3RunConfig config){
        List<ApplicationDate> dateList = config.getApplicationDates();
        List<Variable> variableList = new ArrayList<>();
        Variable[] results = new Variable[dateList.size()];

        for(ApplicationDate date : dateList){
            Variable newVar;
            if(CropEvaluator.hasObjective(date, config.getObjectives())) {
                Objective obj = CropEvaluator.fetchObjective(date, config.getObjectives());
                if(obj.getScalingMin() != null && obj.getScalingMax() != null)
                    newVar = new RealVariableSpecs(
                            date.getPeriod().getType().name(),
                            obj.getScalingMin(),
                            obj.getScalingMax()
                    );
                else
                    newVar = new RealVariableSpecs(
                            date.getPeriod().getType().name(),
                            date.getPeriod().getAppliedMin(),
                            date.getPeriod().getAppliedMax()
                    );
            }else
                newVar = new RealVariableSpecs(
                        date.getPeriod().getType().name(),
                        date.getPeriod().getAppliedMin(),
                        date.getPeriod().getAppliedMax()
                );
            variableList.add(newVar);
        }

        variableList.toArray(results);

        return results;
    }


    public static OptimizationProblem.Constraint[] generateDummyConstraints(DssatNsga3RunConfig config){

        OptimizationProblem.Constraint[] dummyConstraints = new
                OptimizationProblem.Constraint[config.getConstraints().size()];

        // We make this dummy constraint because the optimization code expects you to calculate constraints by hand
        for(OptimizationProblem.Constraint constraint : dummyConstraints){
            constraint = new OptimizationProblem.Constraint(OptimizationProblem.Constraint.INEQUALITY,"0");
        }

        return dummyConstraints;

    }






}
