package org.dsi.dssatnsga;

import emo.Individual;
import emo.OptimizationProblem;
import net.sourceforge.jeval.EvaluationException;
import org.dsi.dssat4j.beans.DatedList;
import org.dsi.dssat4j.beans.PlantGrowthDatum;
import org.dsi.dssatnsgaconfig.ApplicationDate;
import parsing.IndividualEvaluator;

import java.util.List;

public class DetailedIndividual extends Individual  {

    public DetailedIndividual(OptimizationProblem problem,
                              IndividualEvaluator individualEvaluator) throws EvaluationException {
        super(problem, individualEvaluator);
    }

    public DetailedIndividual(OptimizationProblem problem,
                              IndividualEvaluator individualEvaluator,
                              double[] realDesignVariables) throws EvaluationException {
        super(problem, individualEvaluator, realDesignVariables);
    }

    public DetailedIndividual(Individual individual) {
        super(individual);
    }


    private int generation;
    private int runId;
    private int solutionId;
    private double[] objectives;
    private List<ApplicationDate>  dates;
    private DatedList<PlantGrowthDatum> growthData;

    public void setGeneration(int generation){
        this.generation = generation;
    }

    public void setRunId(int runId) {
        this.runId = runId;
    }

    public void setSolutionId(int solutionId){
        this.solutionId = solutionId;
    }

    public void setDates(List<ApplicationDate> dates){
        this.dates = dates;
    }

    public void setGrowthData(DatedList<PlantGrowthDatum> plantGrowthData){
        this.growthData = plantGrowthData;
    }

    public void setObjectives(double [] objs){
        this.objectives = objs;
    }

    public int getGeneration(){
        return this.generation;
    }

    public int getRunId() {
        return this.runId;
    }

    public int getSolutionId(){
        return this.solutionId;
    }

    public List<ApplicationDate> getDates(){
        return this.dates;
    }


    public DatedList<PlantGrowthDatum> getGrowthData(){
        return this.growthData;
    }

    public double[] getObjectives(){
        return this.objectives;
    }

}
