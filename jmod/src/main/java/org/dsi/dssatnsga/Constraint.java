package org.dsi.dssatnsga;

import org.dsi.dssat4j.beans.ResourceApp;

import java.util.ArrayList;
import java.util.List;

public class Constraint {
    // Indices of variables that this applies to
    private List<Integer> variables;
    private ResourceApp.Substance type;
    private double appliedMin;
    private double appliedMax;

    public Constraint(ResourceApp.Substance type, double appliedMin, double appliedMax, List<Integer> variables){
        this.type = type;
        this.appliedMin = appliedMin;
        this.appliedMax = appliedMax;
        this.variables = variables;
    }

    public Constraint(ResourceApp.Substance type, double appliedMin, double appliedMax, Integer variable){
        this.type = type;
        this.appliedMin = appliedMin;
        this.appliedMax = appliedMax;
        this.variables = new ArrayList<>();
        this.variables.add(variable);
    }

    public Constraint(ResourceApp.Substance type, double appliedMin, double appliedMax){
        this.type = type;
        this.appliedMin = appliedMin;
        this.appliedMax = appliedMax;
        this.variables = new ArrayList<>();
    }


    public List<Integer> getVariables(){
        return this.variables;
    }

    public void addVariable(Integer var){
        this.variables.add(var);
    }

    public double getAppliedMin(){
        return this.appliedMin;
    }

    public double getAppliedMax(){
        return this.appliedMax;
    }

    public ResourceApp.Substance getType(){
        return this.type;
    }

    @Override
    public boolean equals(Object other){
        // Check if they're the same class
        if(!(other instanceof Constraint)) return false;

        // Check if the variables match
        if(!(this.variables.equals(((Constraint) other).getVariables()))) return false;

        // Check if the type is equal
        if(!(this.type.equals(((Constraint) other).getType()))) return false;

        // Check if the applied min is equal
        if(!(this.getAppliedMin() == ((Constraint) other).getAppliedMin())) return false;

        // Check if the applied max is equal
        if(!(this.getAppliedMax() == ((Constraint) other).getAppliedMax())) return false;

        return true;
    }

    @Override
    public int hashCode(){
        int sumOfVariables = this.variables.stream().mapToInt(i -> i.intValue()).sum();
        return sumOfVariables;
    }

}
