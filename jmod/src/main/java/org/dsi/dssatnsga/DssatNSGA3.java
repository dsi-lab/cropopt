/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dsi.dssatnsga;


import distancemetrics.DistanceMetric;
import distancemetrics.PerpendicularDistanceMetric;
import emo.DoubleAssignmentException;
import emo.OptimizationProblem;
import engines.AbstractGeneticEngine;
import engines.UnifiedNsga3Engine;

import emo.Variable;
import org.dsi.dssat4j.exceptions.FileParseException;
import org.dsi.dssatnsgaconfig.DssatNsga3RunConfig;
import org.dsi.dssatnsgaconfig.Nsga3ConfigGenerators;
import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 *
 * @author kroppian
 *
 */
public class DssatNSGA3 {


    // The following finals(constants) are the potentials values for the
    // useLocalSearch flag.
    public static final String USAGE = "Usage: DssatNSGA3 \"/path/to/config.json\"";
    public static String outputPath;

    public static void main(String[] args) {


        if(args.length != 1){
            System.err.println(USAGE);     
            System.exit(1);
        }

        String urlString = args[0];

        DssatNsga3RunConfig config = null;
        try{
            config = new DssatNsga3RunConfig(urlString);
        }catch(IOException | FileParseException e){
            System.err.println("Error reading DSSAT experiment:");
            System.err.println(e.getMessage());
            System.exit(1);
        }catch(JSONException e){
            System.err.println("Error parsing [" + urlString + "]");
            System.err.println(e.getMessage());
            System.exit(1);
        }


        int runsCount = config.getRuns();

        String runType;
        if(runsCount == 1)
            runType = "singleLevel";
        else
            runType = "biLevel";

        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-kkmm");
        String time = sdf.format(cal.getTime());
        String runId =   time + String.format( "-%s-P%04d-G%04d/",
                    runType ,
                    config.getPopulation(),
                    config.getGenerations());

        File topOutDir = new File(config.getOutputDir());
        File outputDir = new File(topOutDir + File.separator  + runId);
        PrinterHook hook = new PrinterHook(outputDir.getPath(), config);

        for (int runIndex = 0; runIndex < runsCount; runIndex++) {

            /* Set up variables */
            Variable [] variables = Nsga3ConfigGenerators.generateVariable(config);

            /* Set up objectives */
            OptimizationProblem.Objective[] objectives = new OptimizationProblem.Objective[config.getObjectives().size()];

            /* Set up constraints */
            OptimizationProblem.Constraint[] constraints = Nsga3ConfigGenerators.generateDummyConstraints(config);

            // TODO get threads back
            OptimizationProblem prob = new OptimizationProblem(
                   "cropOptimization",       // problemID
                   variables,                // variablesSpecs
                   objectives,               // objectives
                   constraints,         // constraints
                   12,                       // steps
                   false,                   // adaptive
                   config.getPopulation(),                      // populationSize
                   config.getGenerations(),  // generationsCount
                   1.0,                      // realCrossoverProbability
                   0,                        // realCrossoverDistIndex
                   0.1667,                   // realMutationProbability
                   0,                        // realMutationDistIndex
                   0,                      // binaryCrossoverProbability (no binary vars)
                   0,                    // binaryMutationProbabilty (no binary vars)
                   0.0, // No custom variables
                   0.0, // No custom variables
                   0.5,                      // seed
                   config.getThreads());     // threads


            // Make directories
            if (!outputDir.exists()) {
                outputDir.mkdirs();
            }

            prob.hook = hook;
            CropEvaluator evaluator = null;
            evaluator = new CropEvaluator(prob, config, hook);


            DistanceMetric dm = new PerpendicularDistanceMetric();

            AbstractGeneticEngine engine = null;
            engine = new UnifiedNsga3Engine(prob, evaluator, dm);

            System.out.println("*****************");
            System.out.format("    Run(%03d)  %n", runIndex);
            System.out.println("*****************");
            // Create output directory of this specific run
            File runOutputDir = new File(outputDir.getPath() + File.separator + String.format("run%03d/", runIndex));
            if (!runOutputDir.exists()) {
                runOutputDir.mkdir();
            }

            // Start the engine
            try{
                engine.start(runOutputDir, runIndex, 0, Double.MAX_VALUE, Integer.MAX_VALUE);
            }catch(IOException e){
                System.err.println("IO exception during optimization run: " + e.getMessage());
                e.printStackTrace();
                System.exit(1);
            }catch(DoubleAssignmentException e){
                System.err.println("Double assignment exception during optimization run: " + e.getMessage());
                e.printStackTrace();
                System.exit(1);
            }catch(RuntimeException e){
                System.err.println("Runtime exception during optimization: " + e.getMessage());
                e.printStackTrace();
                System.exit(1);
            }


            System.out.println("*** EVAL COUNT = " + evaluator.getFunctionEvaluationsCount());
            // Reset the number of function evaluations to start
            // counting from Zero again in the next iteration
            evaluator.resetFunctionEvaluationsCount();

            config.regenerateDates();
            hook.nextRun();

        }



    }
}

