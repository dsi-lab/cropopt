package org.dsi.dssatnsga;

import org.dsi.dssat4j.beans.ResourceApp;

import java.util.*;

public class Constraints implements Iterable<Constraint>{

    private class ConstraintIterator implements Iterator<Constraint>{

        final Iterator<Constraint> iter = constraints.iterator();

        @Override
        public boolean hasNext(){
            return iter.hasNext();
        }

        @Override
        public Constraint next() {
            return iter.next();
        }

    }

    @Override
    public Iterator<Constraint> iterator() {
        return new ConstraintIterator();
    }

    private Set<Constraint> constraints;

    public Constraints(){
        this.constraints = new HashSet<Constraint>();
    }
    public void addConstraintPeriod(ResourceApp.Substance type, double min, double max, List<Integer> variables){
        constraints.add(new Constraint(type, min, max, variables));

    }

    public void addConstraint(Constraint constraint){
        constraints.add(constraint);
    }

    public int size(){
        return this.constraints.size();
    }

    public Set<Constraint> toSet() {
        return this.constraints;
    }

    public void append(Constraints newConstraints){
        this.constraints.addAll(newConstraints.toSet());
    }


}
