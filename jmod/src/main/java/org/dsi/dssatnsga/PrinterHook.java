package org.dsi.dssatnsga;

import emo.Individual;
import utils.GenerationalHook;
import org.apache.commons.io.FileUtils;
import org.dsi.dssat4j.beans.DatedItem;
import org.dsi.dssat4j.beans.DatedList;
import org.dsi.dssat4j.beans.PlantGrowthDatum;
import org.dsi.dssat4j.tools.DssatTools;
import org.dsi.dssatnsgaconfig.ApplicationDate;
import org.dsi.dssatnsgaconfig.DssatNsga3RunConfig;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PrinterHook extends GenerationalHook {

    private Integer runId;
    private Integer current_id;
    private Integer generation;
    private List<DetailedIndividual> currentIndvs;
    private List<DetailedIndividual> globalIndvs;
    private String outputDirPath;
    private DssatNsga3RunConfig config;

    PrinterHook(String outputDir, DssatNsga3RunConfig config) {
        this.runId = 0;
        this.current_id = 0;
        this.generation = 0;
        this.currentIndvs = new ArrayList<>();
        this.outputDirPath = outputDir;
        this.config = config;
        this.globalIndvs = new ArrayList<DetailedIndividual>();
    }

    void addSolution(Individual ind, DatedList<PlantGrowthDatum> growthData, double[] objs, List<ApplicationDate> dates) {
        synchronized (this) {
            DetailedIndividual detailedIndividual = new DetailedIndividual(ind);
            detailedIndividual.setSolutionId(this.current_id);
            detailedIndividual.setRunId(this.runId);
            detailedIndividual.setGeneration(this.generation);
            detailedIndividual.setDates(dates);
            detailedIndividual.setGrowthData(growthData);
            detailedIndividual.setObjectives(objs);
            int i = 0;
            for(double obj : objs){
                detailedIndividual.setObjective(i,obj);
                i++;
            }
            this.currentIndvs.add(detailedIndividual);
            this.current_id++;
        }
    }

    @Override
    public void run(Individual[] sols)throws IOException{

        StringBuilder output = new StringBuilder();

        this.globalIndvs.addAll(this.currentIndvs);

        DetailedIndividual[] globalIndvsArr = new DetailedIndividual[this.globalIndvs.size()];

        this.globalIndvs.toArray(globalIndvsArr);

        List<List<DetailedIndividual>> fronts = rankIndividuals(globalIndvsArr, 0, "yup");

        this.globalIndvs = fronts.get(0);


        /*
         Build the header
          */
        if(this.generation == 0 && this.runId == 0){
            output.append("run_id,gen_id,solution_id,");

            for(int i = 0; i < this.globalIndvs.get(0).getObjectivesCount(); i++){
                output.append("objective_").append(i).append( ",");
            }

            for(int i = 0; i < this.globalIndvs.get(0).real.length; i++){
                output.append("variable_").append(i).append(",");
            }

            for(int i = 0; i < this.globalIndvs.get(0).getDates().size(); i++){
                output.append("date_").append(i).append(",");
            }

            for(DatedItem date : this.globalIndvs.get(0).getGrowthData()){
                output.append("growth_day_").append(DssatTools.convert2YYDDD(date.getDate()).substring(2,5)).append(",");
            }

            output.append("\n");
            
            File outputFile = new File(this.outputDirPath+ File.separator + "full_data"  + ".csv");
            Charset charset = null;

            FileUtils.writeStringToFile(outputFile,output.toString(),charset);

        }

        output = new StringBuilder(); 

        if((this.generation + 1) == this.config.getGenerations()) {

            /*
            Print nondoniminated individuals
             */
            for(DetailedIndividual ind : this.globalIndvs){

                output.append(this.runId).append(",");

                output.append(this.generation).append(",");

                output.append(ind.getRunId()).append("_").append(ind.getGeneration()).append("_").append(ind.getSolutionId()).append(",");

                for(Double objective : ind.getObjectives()){
                    output.append(objective).append(",");
                }

                int j = 0;
                for(Double variable : ind.real){
                    // I need application dates and objectives
                    int realVariableValue = CropEvaluator.calculateRealValue(variable,
                            this.config.getObjectives(),
                            config.getApplicationDates().get(j));
                    output.append(realVariableValue).append(",");
                    j++;
                }

                for(ApplicationDate date : ind.getDates()){
                    output.append(DssatTools.convert2YYDDD(date.getDate()).substring(2,5)).append("," );
                }

                for(DatedItem rawDatum : ind.getGrowthData()){
                    PlantGrowthDatum datum = (PlantGrowthDatum) rawDatum.getItem();
                    output.append(datum.getTopsWeight()).append(",");
                }

                output.append("\n");
            }

            File outputFile = new File(this.outputDirPath+ File.separator + "full_data"  + ".csv");
            Charset charset = null;

            FileUtils.writeStringToFile(outputFile,output.toString(),charset, true);

        }


        this.generation++;
        this.currentIndvs = new ArrayList<>();


    }

    public void nextRun(){
        this.generation = 0;
        this.runId++;
    }

    // I took this from the unsga-3 code. I would have like to use it directly, but I couldn't get it cooperate.
    // TODO make more elegant.
    public static List<DetailedIndividual> getNonDominatedIndividuals(List<DetailedIndividual> individuals, double epsilon) {
        List<DetailedIndividual> nonDominatedList = new ArrayList<DetailedIndividual>();
        outerLoop:
        for (int i = 0; i < individuals.size(); i++) {
            if (!individuals.get(i).isFeasible() && individuals.get(i).getRank() == 1) {
                // This condition is useful only in the case if all the
                // solutions are infeasible and the solution occupying the
                // first front has other copies. This if statements protects
                // this individual from being removed. (Remember: there cannot
                // be more than one infeasible individual in any front at the
                // same time).
                nonDominatedList.add(individuals.get(i));
                continue outerLoop;
            }
            for (int j = 0; j < individuals.size(); j++) {
                if (i != j) {
                    if (individuals.get(j).dominates(individuals.get(i), epsilon)) {
                        continue outerLoop;
                    }
                }
            }
            nonDominatedList.add(individuals.get(i));
        }
        return nonDominatedList;
    }

    public List<List<DetailedIndividual>> rankIndividuals(DetailedIndividual[] individuals, double epsilon, String optionalDisplayMessage) {
        int flag;
        int i;
        int end;
        int front_size;
        int rank = 1;
        ArrayList<DetailedIndividual> orig = new ArrayList<DetailedIndividual>();
        ArrayList<DetailedIndividual> cur = new ArrayList<DetailedIndividual>();
        int temp1;
        int temp2;
        for (i = 0; i < individuals.length; i++) {
            orig.add(individuals[i]);
        }
        do {
            if (orig.size() == 1) {
                orig.get(0).setRank(rank);
                orig.get(0).validRankValue = true;
                break;
            }
            cur.add(0, orig.remove(0));
            temp1 = 0;
            temp2 = 0;
            front_size = 1;
            do {
                temp2 = 0;
                do {
                    end = 0;
                    flag = checkDominance(orig.get(temp1), cur.get(temp2), epsilon);
                    if (flag == 1) {
                        orig.add(0, cur.remove(temp2));
                        temp1++;
                        front_size--;
                    } else if (flag == -1) {
                        end = 1;
                    } else {
                        temp2++;
                    }
                } while (end != 1 && temp2 < cur.size());
                if (flag == 0 || flag == 1) {
                    cur.add(0, orig.get(temp1));
                    temp2++;
                    front_size++;
                    orig.remove(temp1);
                }
                if (flag == -1) {
                    temp1++;
                }
            } while (temp1 < orig.size());
            for (DetailedIndividual individual : cur) {
                individual.setRank(rank);
                individual.validRankValue = true;
            }
            cur.clear();
            rank++;
        } while (!orig.isEmpty());
        // Prepare return List of Lists
        List<List<DetailedIndividual>> fronts = new ArrayList<List<DetailedIndividual>>();
        // Get Max rank
        int maxRank = 1;
        for (DetailedIndividual individual : individuals) {
            if (individual.getRank() > maxRank) {
                maxRank = individual.getRank();
            }
        }
        // Initialize the fronts lists
        for (int f = 0; f < maxRank; f++) {
            fronts.add(new ArrayList<DetailedIndividual>());
        }
        // Add each individual to its corresponding rank
        for (DetailedIndividual individual : individuals) {
            fronts.get(individual.getRank() - 1).add(individual);
        }

        // Return the final list of fronts
        return fronts;
    }

    int checkDominance(DetailedIndividual individual1, DetailedIndividual individual2, double epsilon) {
        if (individual1.dominates(individual2, epsilon)) {
            return 1;
        } else if (individual2.dominates(individual1, epsilon)) {
            return -1;
        } else if(Arrays.equals(individual1.getObjectives(),individual2.getObjectives())){ // Hacking this so we don't get duplicates in the long run
            return -1;
        } else {
            return 0;
        }

    }


}
