package org.dsi.dssatnsga;

public class Objective {
    private String type;
    private Double scalingMin;
    private Double scalingMax;

    public Objective(String type){
        this.type = type;
        this.scalingMax = null;
        this.scalingMin = null;
    }

    public Objective(String type, Double scalingMin, Double scalingMax){
        this.type = type;
        this.scalingMax = scalingMax;
        this.scalingMin = scalingMin;
    }

    public String getType(){
        return this.type;
    }

    public Double getScalingMin(){
        return this.scalingMin;
    }

    public Double getScalingMax(){
        return this.scalingMax;
    }

    public void setScalingMin(Double min){
        this.scalingMin = min;
    }

    public void setScalingMax(Double max){
        this.scalingMax = max;
    }

    @Override
    public boolean equals(Object obj){
        String className = Objective.class.getSimpleName();
        if(obj instanceof Objective)
            return ((Objective) obj).getType().equals(this.type);
        else if(obj instanceof String)
            return ((String) obj).equals(this.type);
        else
            return false;
    }

    @Override
    public String toString(){
        return this.type;
    }

}
