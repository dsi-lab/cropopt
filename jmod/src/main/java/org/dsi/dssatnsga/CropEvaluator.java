package org.dsi.dssatnsga;

import emo.Individual;
import emo.OptimizationProblem;
import parsing.IndividualEvaluator;

import org.dsi.dssat4j.IO.Experiment;
import org.dsi.dssat4j.IO.Parsers;
import org.dsi.dssat4j.beans.Economics;
import org.dsi.dssat4j.beans.IrrigationApps;
import org.dsi.dssat4j.beans.FertilizerApps;
import org.dsi.dssat4j.beans.Treatment;
import org.dsi.dssat4j.exceptions.*;
import org.dsi.dssat4j.tools.DssatConfig;
import org.dsi.dssat4j.IO.Outcome;
import org.dsi.dssat4j.tools.DssatRunner;
import org.dsi.dssat4j.tools.DssatSession;
import org.dsi.dssat4j.tools.DssatSessionFactory;
import org.dsi.dssatnsgaconfig.ApplicationDate;
import org.dsi.dssatnsgaconfig.DssatNsga3RunConfig;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author kroppian
 */
public class CropEvaluator extends IndividualEvaluator {

    private DssatConfig dssatConfig;
    private DssatSession session;
    private Constraints constraints;
    private List<ApplicationDate> dates;
    private OptimizationProblem problem;
    private List<Objective> objectives;
    private Economics economics;
    private File runDirectory;
    private File profile;
    private File experiment;
    private File executable;
    private PrinterHook printerHook;
    private int logLevel;

    private boolean scaled = false;

    public CropEvaluator(OptimizationProblem problem, DssatNsga3RunConfig runConfig, PrinterHook hook)  {

        this(problem);
        this.printerHook = hook;

        this.constraints = runConfig.getConstraints();
        this.dates = runConfig.getApplicationDates();
        this.runDirectory = new File(runConfig.getRunDirectory());
        this.objectives = runConfig.getObjectives();
        this.experiment = new File(runConfig.getExperimentFilePath());
        this.profile = new File(runConfig.getProfilePath());
        this.executable = new File(runConfig.getExecutablePath());
        this.logLevel = runConfig.getLogLevel();

        if(this.objectives.size() != problem.objectives.length)
            throw new RuntimeException("The number of objectives given does not equal the number of problem objectives " +
                    "given (" + this.objectives.size() + " vs " + problem.objectives.length + ")" );



        // Set up a DSSAT configuration file
        this.dssatConfig = null;

        try{
            this.dssatConfig =  new DssatConfig(this.runDirectory, this.executable, this.profile);
        }catch(IOException e){
            throw new RuntimeException(String.format("Error processing configuration: %s\n", e.getMessage()));
        }

        // Set up the DSSAT session that will persist throughout the optimization
        DssatSessionFactory seshFact = new DssatSessionFactory();

        try{
            this.session = seshFact.createSession(this.dssatConfig);
        }catch(SessionException e){
            throw new RuntimeException(String.format("Error setting up session: %s\n", e.getMessage()));
        }


        /*this.dssatConfig = new DssatConfig(this.runDirectory,
                this.executable,
                DssatConfig.CropTypes.MAIZE, 
                config.getInstitutionCode(), 
                config.getSiteCode(), 
                config.getYear(), 
                String.format("%02d",config.getExperimentNumber()));*/


    }

    private CropEvaluator(OptimizationProblem problem) {
        this.problem = problem;
    }

    public void updateIndividualObjectivesAndConstraints( OptimizationProblem problem, Individual individual) {

        // Calculate objective values
        double[] obj = getObjectives(individual);

        // Copy objective values to the individual
        for (int i = 0; i < obj.length; i++) {
            individual.setObjective(i, obj[i]); }

        // Increase Evaluations Count by One (counted per individual)
        funEvaCount++;

        // Announce that objective function values are valid
        individual.validObjectiveFunctionsValues = true;

        // Evaluate the final expression and store the results as the individual's constraints values.

        double runningTotal;
        double periodMax;
        double periodMin;
        int constIndex = 0;
        for(Constraint constraint : constraints){
            runningTotal = 0;
            periodMax = constraint.getAppliedMax();
            periodMin = constraint.getAppliedMin();
            for(int var : constraint.getVariables()){
                runningTotal += calculateRealValue(individual.real[var], this.objectives, this.dates.get(var));
            }

            if(runningTotal > periodMax) {
                if(logLevel > 0)
                  System.out.println("    total for " + constraint.getType() + " is " + runningTotal + " and is larger than " + periodMax + " (penalty " + (periodMax - runningTotal)  + ")");
                individual.setConstraintViolation(constIndex, periodMax - runningTotal);
            } else if (runningTotal < periodMin) {
                if(logLevel > 0)
                  System.out.println("    total for " + constraint.getType() + " is " + runningTotal + " and is less than " + periodMin + " (penalty " + (runningTotal - periodMin) + ")");
                individual.setConstraintViolation(constIndex, runningTotal - periodMin);
            } else{
                if(logLevel > 0)
                  System.out.println("    total for " + constraint.getType() + " is " + runningTotal + " and is in range. ");
                individual.setConstraintViolation(constIndex, 0);
            }


            constIndex++;
        }

        // Announce that objective function values are valid
        individual.validConstraintsViolationValues = true;


    }

    private double[] getObjectives(Individual inds) {


        double rawVariables[] = inds.real;
        double rawObjectives[] = new double[this.problem.objectives.length];

        // This object represents the experiment that we want to run
        Experiment exp = null;
        // Try to open the existing experiment file
        try {
            exp = Parsers.parseExpFile(this.experiment);
        }catch(IOException e){
            throw new RuntimeException(String.format("Cannot read experiment file:\n %s\n", e.getMessage()));
        }catch(FileParseException e){
            throw new RuntimeException(String.format("Cannot parse experiment file:\n %s\n", e.getMessage()));
        }

        // Create a new object to represent the irrigation scheme we want to try
        IrrigationApps irrigationData = new IrrigationApps();
        FertilizerApps fertilizerData = new FertilizerApps();

        FertilizerApps originalFertilizerData = exp.getFertilizerLevel(1);
        IrrigationApps originalIrrigationData = exp.getIrrigationLevel(1);

        Integer totalIrrigation = null;
        Integer totalNitrogen = null;
        Integer totalPhosphorus = null;
        Integer totalPotash = null;

        // Here, we recording the totals of various amounts (yield, water usage, nitrogen)
        int i = 0;
        int realWorldValue;
        for(ApplicationDate date : this.dates){
            try{
                realWorldValue = calculateRealValue(rawVariables[i], this.objectives, date);
                switch(date.getType()){
                    case WATER:
                        irrigationData.addIrrApp(date.getDate(), date.getMethod(), realWorldValue);
                        if(totalIrrigation == null) totalIrrigation = 0;
                        if(getObjectiveTypes(this.objectives).contains("TOTAL_" + date.getType().toString()))
                            totalIrrigation += realWorldValue;
                        break;
                    case NITROGEN:
                        fertilizerData.addNirtroApp(date.getDate(), date.getMethod(), realWorldValue, date.getApplicationDepth());
                        if(totalNitrogen == null) totalNitrogen = 0;
                        if(getObjectiveTypes(this.objectives).contains("TOTAL_" + date.getType().toString()))
                            totalNitrogen += realWorldValue;
                        break;
                    case PHOSPHORUS:
                        fertilizerData.addPhosApp(date.getDate(), date.getMethod(), realWorldValue, date.getApplicationDepth());
                        if(totalPhosphorus == null) totalPhosphorus = 0;
                        if(getObjectiveTypes(this.objectives).contains("TOTAL_" + date.getType().toString()))
                            totalPhosphorus += realWorldValue;
                        break;
                    case POTASSIUM:
                        fertilizerData.addPotApp(date.getDate(), date.getMethod(), realWorldValue, date.getApplicationDepth());
                        if(totalPotash == null) totalPotash = 0;
                        if(getObjectiveTypes(this.objectives).contains("TOTAL_" + date.getType().toString()))
                            totalPotash += ((Long) Math.round(rawVariables[i])).intValue();
                        break;
                    default:
                        throw new RuntimeException("Given application type not recognized.");

                }

                i++;

            }catch(IllegalStringFormatException | IllegalApplicationException e){
                System.err.println(e.getMessage());
                throw new RuntimeException("Error processing application data.");
            }
        }

        // Clear the old treatments from the experiment file
        Treatment treatment = exp.getTreatment(1);
        treatment.setIrrigationLevel(1);
        treatment.setFertilizerLevel(1);
        treatment.setTreatmentNumber(1);

        exp.clearTreatments();
        if(getObjectiveTypes(this.objectives).contains("TOTAL_WATER")) exp.clearIrrigationLevels();
        //if(totalNitrogen != null) exp.clearFertilizerLevels();
        if(getObjectiveTypes(this.objectives).contains("TOTAL_NITROGEN")) exp.clearFertilizerLevels();


        // Plug the irrigation and fertilizer data into our experiment
        exp.setTreatment(treatment);
        if(getObjectiveTypes(this.objectives).contains("TOTAL_NITROGEN"))
            exp.setFertilizerLevel(1, fertilizerData);
        else
            exp.setFertilizerLevel(1, originalFertilizerData);

        if(getObjectiveTypes(this.objectives).contains("TOTAL_WATER"))
            exp.setIrrigationLevel(1, irrigationData);
        else
            exp.setIrrigationLevel(1, originalIrrigationData);


        Outcome outcome;
        try{
            DssatRunner runner = this.session.getDssatRunner();
            // Run the experiment file you just created with DSSAT
            getObjectiveTypes(this.objectives).contains("YIELD");
            outcome = runner.run(exp,1);
        }catch(FailedRunException e){
            throw new RuntimeException("Run failed. See WARNING.OUT\n" + e.getMessage());
        }catch(SessionException e){
            throw new RuntimeException(String.format("Failed to set up runner: %s ", e.getMessage()));

        }

        int index;
        for(Objective objective : this.objectives){
            index = getObjectiveTypes(this.objectives).indexOf(objective);
            String objectiveType = objective.getType();
            if(index == -1)
                continue;

            switch(objectiveType){
                case "YIELD" :
                    rawObjectives[index] = - outcome.getYield();
                    break;
                case "GROSS_MARGIN" :
                    rawObjectives[index] = - this.economics.calculateGrossMargin(1,outcome);
                    break;
                case "LEACHING_NITROGEN" :
                    rawObjectives[index] = outcome.getNitroLeaching();
                    break;
                case "TOTAL_WATER" :
                    rawObjectives[index] = totalIrrigation;
                    break;
                case "TOTAL_PHOSPHORUS":
                    rawObjectives[index] = totalPhosphorus;
                    break;
                case "TOTAL_NITROGEN":
                    rawObjectives[index] = totalNitrogen;
                case "STOCHASTIC_YIELD_MEAN":
                    break;
                case "STOCHASTIC_YIELD_VARIANCE":
                    break;
                default:
                    break;
            }

        }


        // Check total values are consistant
        if(totalIrrigation != null && totalIrrigation != outcome.getIrrigationApplied())
            throw new RuntimeException(String.format("Input total irrigation [%d] does not equal the output " +
                                                            "total irrigation according to DSSAT [%f]",
                                                            totalIrrigation, outcome.getIrrigationApplied() ));

        if(totalNitrogen != null && totalNitrogen != outcome.getNitrogenApplied())
            throw new RuntimeException(String.format("Input total nitrogen [%d] does not equal the output " +
                            "total nitrogen according to DSSAT [%f]",
                    totalNitrogen, outcome.getNitrogenApplied() ));

        int j = 0;
        if(this.logLevel == 1){
          for(Objective objectiveCat : this.objectives){
              System.out.print(objectiveCat.getType() + ": " + String.format("%6.1f",rawObjectives[j]) + "; ");
              j++;
          }
          System.out.println();
        }

        this.printerHook.addSolution(inds, outcome.getPlantGrowth(), rawObjectives, this.dates);

        return rawObjectives;
    }

    public static double convertScaling(double value, Objective obj, ApplicationDate date){
        double result;

        if(obj.getScalingMax() == null || obj.getScalingMin() == null)
            result = value;
        else {
            // perform linear interpolation
            double x0 = obj.getScalingMin();
            double x1 = obj.getScalingMax();
            double y0 = date.getApplicationMin();
            double y1 = date.getApplicationMax();
            double x = value;
            result = y0 + (x - x0)*((y1 - y0)/(x1 - x0));

        }

        return result;
    }

    public static int calculateRealValue(double originalValue, List<Objective> objectives, ApplicationDate date){
        Objective objective;
        int realWorldValue;
        if(hasObjective(date, objectives)) {
            objective = fetchObjective(date, objectives);
            realWorldValue = (int) Math.round(convertScaling(originalValue, objective, date));
        }else
            realWorldValue = (int) Math.round(originalValue);

        return realWorldValue;
    }

    public static Objective fetchObjective(ApplicationDate date, List<Objective> objectives){
        String objectiveType = "TOTAL_" + date.getType().toString();
        return objectives.get(getObjectiveTypes(objectives).indexOf(objectiveType));
    }


    public static boolean hasObjective(ApplicationDate date, List<Objective> objectives){
        String objectiveType = "TOTAL_" + date.getType().toString();
        return getObjectiveTypes(objectives).contains(objectiveType);

    }


    public static List<String> getObjectiveTypes(List<Objective> objectives){
        List<String> results = new ArrayList<String>();
        for(Objective objective : objectives){
            results.add(objective.getType());
        }
        return results;
    }

    @Override
    public Individual[] getParetoFront(int objectivesCount, int n) {
        throw new RuntimeException("Pareto front unavailable");
    }

}
