import csv, sys, subprocess, getopt, os;

def parseRefPoint(refStr):
    refStrList = refStr.split(',');    
    refDblList = []
    for ref in refStrList:
        refDblList.append(ref);
    return refDblList

def getHyperVolume(csvFileReader):
    result = "";
    for row in csvFileReader: 
        result += " ".join(row);
        result += "\n";

    return result;

argv = sys.argv[1:]

if len(argv) == 0: 
    print("Usage: calchypervol -o OUTPUTFILE.dat [-r x,y,z,...,n] FILE1 [FILE2 .. FILEN] -o OUTPUTFILE.dat ");            
    exit(1);

try:
    opts, files = getopt.getopt(argv,"hr:o:");
except getopt.GetoptError:
    print("Usage: calchypervol  -o OUTPUTFILE.dat [-r x,y,z,...,n] FILE1 [FILE2 .. FILEN]");            
    exit(2);

refPoints = [];

for opt, arg in opts:
    if opt == '-h':
        print("Usage: calchypervol -o OUTPUTFILE.dat FILE1 [FILE2 .. FILEN] [-r x,y,z,...,n]");            
        exit(0);
    elif opt == '-r': 
        refPoints = parseRefPoint(arg);
    elif opt == '-o': 
        outputFileName = arg;
    else:
        print("Option " + opt + " not recognized.");
        exit(2);

result = "#\n";

for file in files:
    # Find the hypervolume
    with open(file, 'rt') as csvFile:
        reader = csv.reader(csvFile, delimiter=' ',quotechar='|'); 
        result += getHyperVolume(reader) 
        result += "#\n" 

tmpFile="/tmp/pareto_solutions"
out = open(tmpFile,'w');
out.write(result);
out.close();

currentDirectory = os.path.dirname(os.path.realpath(__file__))


wfg_out = subprocess.check_output([ currentDirectory + "/wfg", tmpFile] + refPoints, universal_newlines=True);

out = open(outputFileName, 'w');
out.write(wfg_out);
out.close();


print("success!")

#print(wfg_out);

