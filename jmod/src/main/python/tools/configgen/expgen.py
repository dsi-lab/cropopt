import re, sys, time
from datetime import datetime
from datetime import timedelta

#
# Functions
#############

def fixStr(string, year):


    formats = [{"regex":"YY\d\d\d","directives": "%y%j"}, 
            {"regex":"YYYY-\d\d-\d\d", "directives": "%Y-%m-%d"},
            {"regex":"QQ", "directives": "%y"},
            {"regex":"TTTT", "directives": "%Y"}]

    for formt in formats:

        regex = re.compile(formt["regex"])
        matches = re.search(regex, string)         

        if matches != None: 

            raw_date = matches.group(0)

            # First try to replace a YYYY if it exists
            raw_date = re.sub(r"YYYY", str(year), raw_date)
            # First try to replace a YY if it was left over from the last
            raw_date = re.sub(r"YY", str(year)[-2:], raw_date)
            # Don't forget to look for the QQ
            raw_date = re.sub(r'QQ', str(year)[-2:], raw_date)
            # and TTTT
            raw_date = re.sub(r'TTTT', str(year), raw_date)



            date = datetime.strptime(raw_date, formt["directives"])

            # Check for leap year
            if year % 4 == 0 and date.timetuple().tm_yday > 60:
                date = date + timedelta(days=1)
            
            date_str = date.strftime(formt["directives"])

            #YYddd_new = str(year)[-2:] + str(doy)

            string = re.sub(formt["regex"], date_str, string)

    return string


#
# Main
#############

args = sys.argv[1:]

if len(args) != 2:
    print("Usage: %s TEMPLATE YEAR" % sys.argv[0])
    sys.exit(1)

template_file = args[0]

year = int(args[1])

with open(template_file,'r') as exp_reader:

    line = exp_reader.readline()

    while line != '':

        line = fixStr(line, year)

        sys.stdout.write(line)

        line = exp_reader.readline()






