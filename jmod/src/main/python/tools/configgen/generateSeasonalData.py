import datetime

entry_template="{'type': '%s', 'start-date':'%s', 'end-date':'%s', " + \
        "'applied-min' : %d, 'applied-max' : %d, 'total-applied-min': %d," + \
        "'total-applied-max':%d,'times-applied-max':1,'times-applied-min': 1 },"

start_dates = [datetime.date(1982, 3, 1), datetime.date(1982, 3, 1)]
season_lens = [120, 120]
applied_mins = [0, 0]
applied_maxes = [50, 150]
types = ['water', 'nitrogen']

for app_type in range(0, len(season_lens)):

    date_list = [start_dates[app_type] + datetime.timedelta(days=x) for x in range(0, season_lens[app_type])]

    for date in date_list: 
        print(entry_template % (types[app_type], date.isoformat(), date.isoformat(), applied_mins[app_type], applied_maxes[app_type], applied_mins[app_type], applied_maxes[app_type]))

    print()


# ron 
# slate


