import sys, getopt;
from random import randint;

def usage(code):
    print("Usage: appdategen YEAR REALIZATIONS [OPTIONS]");
    print("Options: ");
    print("     -i NUMBER_OF_IRRIGATION_APPLICATIONS [--imin DOY] [--imax DOY]");
    print("     -n NUMBER_OF_NITROGEN_APPLICATIONS [--nmin DOY] [--nmax DOY]");
    print("     -p NUMBER_OF_PHOSPHORUS_APPLICATIONS [--pmin DOY] [--pmax DOY]");
    exit(code);

def genDate(year, usedDates, minDate, maxDate):
    originalDateCreated=False;
    proposedDate = ""
    while not originalDateCreated:
        proposedDate = str(year)[2:5] + str(randint(minDate, maxDate)).zfill(3)
        if usedDates.count(proposedDate) is 0:
            originalDateCreated = True;
    return proposedDate;


if len(sys.argv) < 3:
    usage(1);

argv = sys.argv[3:]

realizations = 1;

try: 
    realizations = int(sys.argv[2]);
except ValueError:
    usage(1);

year = 0;

try: 
    year = int(sys.argv[1]);
except ValueError:
    usage(1);

if year < 1900 or year > 2100: 
    usage(1);

leapYear = False;
if year % 4 == 0: 
    leapYear = True;

try: 
    opts, files = getopt.getopt(argv,"hi:n:p:",["imin=", "imax=", "nmin=", "nmax=", "pmin=", "pmax="])
except getopt.GetoptError:
    usage(2);

irrApps = 0;
nitroApps = 0;
phosApps = 0;


irrAppMin = nitroAppMin = phosAppMin = 1;
irrAppMax = nitroAppMax = phosAppMax = 366 if leapYear else 365;

for opt, arg in opts: 
    try:
        if opt == '-h':
            usage(0);
        elif opt == '-i':
            irrApps = int(arg); 
        elif opt == '-n':
            nitroApps = int(arg);
        elif opt == '-p':
            phosApps = int(arg);
        elif opt == '--imin':
            irrAppMin = int(arg);
        elif opt == '--imax':
            irrAppMax = int(arg);
        elif opt == "--nmin":
            nitroAppMin = int(arg);
        elif opt == "--nmax":
            nitroAppMax = int(arg);
        elif opt == "--pmin":
            phosAppMin = int(arg);
        elif opt == "--pmax":
            phosAppMax = int(arg);
        else: 
            usage(2);
    except ValueError:
        print("Error reading value " + arg + " for option " +opt )
        exit(1);

if irrApps + nitroApps + phosApps == 0:
    usage(1);


for realization in range(0, realizations):
    usedDates = [];
    sys.stdout.write("WATER," );
    for date in range(0, irrApps):
        #                 year                           day of year
        proposedDate = genDate(year, usedDates, irrAppMin, irrAppMax);
        sys.stdout.write(proposedDate + ",");
        usedDates.append(proposedDate);
    print("");
    usedDates = [];

    sys.stdout.write("NITROGEN,");
    for date in range(0, nitroApps):
        #                 year                           day of year
        proposedDate = genDate(year, usedDates, nitroAppMin, nitroAppMax);
        sys.stdout.write(proposedDate + ",");
        usedDates.append(proposedDate);
    print("");
    usedDates = [];

    sys.stdout.write("PHOSPHORUS,");
    for date in range(0, phosApps):
        #                 year                           day of year
        proposedDate = genDate(year, usedDates, phosAppMin, phosAppMax);
        sys.stdout.write(proposedDate + ",");
        usedDates.append(proposedDate);
    print("");



