import csv, sqlite3, sys, io, re, warnings;


def init_db(cur):
    cur.execute('''

        CREATE TABLE solutions(
            solution_id INTEGER,
            file_no INTEGER,
            generation_id INTEGER,
            PRIMARY KEY (solution_id)
        );

    ''')
    cur.execute(''' 
        CREATE TABLE objectives(
            solution_id INTEGER,
            objective_id INTEGER, 
            objective FLOAT,
            FOREIGN KEY(solution_id) REFERENCES solutions(solution_id)

        );
            
    ''')

    cur.execute(''' 
        CREATE TABLE variables(
            solution_id INTEGER,
            variable_id INTEGER,
            variable FLOAT,
            FOREIGN KEY(solution_id) REFERENCES solutions(solution_id)

        );
            
    ''')

    cur.execute(''' 
        CREATE TABLE dates(
            solution_id INTEGER,
            date_id INTEGER,
            date INTEGER,
            FOREIGN KEY(solution_id) REFERENCES solutions(solution_id)
        );
            
    ''')

    cur.execute(''' 
        CREATE TABLE growth(
            solution_id INTEGER,
            growth_day INTEGER,
            growth_amount FLOAT,
            FOREIGN KEY(solution_id) REFERENCES solutions(solution_id)
        );
            
    ''')


def getCsvSolutionIds(csvFilePath):
    solutionIds = [];
    with open(csvFilePath, 'r') as csvfile:
        csvreader = csv.DictReader(csvfile);
        for solution in csvreader:
            solutionIds.append(int(solution['solution_id'])); 

    return solutionIds;


def concatDbA2CsvB(dbA, csvFileB, fileNo): 

    solutionsA = getSolutionIds(dbA);
    solutionsB = [];

    if len(solutionsA) is 0:
        solutionId = 0;
    else:
        solutionId = max(solutionsA) + 1; 

    for solution in csvFileB:

        solutionsB.append(solutionId);

        for fields in solution:
            if solution[fields] is None:
                raise LookupError("Missing field(s) in the %dth row of csv file." % (solutionId + 1) );

        try:
            genId = int(solution['gen_id']);
        except KeyError: 
            genId = 0;


        # Adding solution
        dbA.cursor().execute( '''
            INSERT INTO solutions(
                solution_id,
                file_no,
                generation_id
            )
            values (
                {0},
                {1},
                {2}
            );

                
        '''.format(solutionId, fileNo, genId))
    
        # Adding objectives
        allObjectivesFound = False;
        currentObjInd = 0;
        while not allObjectivesFound:
            try:

                addObjective(dbA.cursor(), solutionId, currentObjInd,float(solution['objective_' + str(currentObjInd)]));
           
            except KeyError: 
                allObjectivesFound = True;
                if currentObjInd == 0:
                    warnings.warn("No objectives found in file %d" % fileNo, UserWarning)
    
            currentObjInd += 1;

        # Adding Growth 

        # First find the first growth date
        growthDate = 0;
        firstDateFound = False;
        while not firstDateFound and growthDate < 365:
            try:
                solution['growth_day_' + str(growthDate).zfill(3)];
                firstDateFound = True;
            except KeyError:
                growthDate += 1;

        # If growth data is found
        if growthDate < 365:

            lastDateReached = False;
           
            ## Next write the growth date
            while not lastDateReached:
                try:
          
                    addGrowthDate(dbA.cursor(), solutionId, growthDate , solution['growth_day_' + str(growthDate).zfill(3)])

                    growthDate += 1;
                     
                except KeyError:
                    lastDateReached = True;

        else:
            warnings.warn("No growth dates found in file %d" % fileNo, UserWarning)


        # Adding dates
        allDatesFound = False;
        dateId = 0;
        while not allDatesFound:
            try:

                addDate(dbA.cursor(), solutionId, dateId, solution['date_' + str(dateId)]);

                dateId += 1;

            except KeyError:
                allDatesFound = True;
        

        if dateId == 0: 
            warnings.warn("No dates found in file %d" % fileNo, UserWarning);


        # Adding Variables 
        allVariablesFound = False;
        variableId = 0;
        while not allVariablesFound:
            try:

                addVariable(dbA.cursor(), solutionId, variableId, solution['variable_' + str(variableId)]);

                variableId += 1;

            except KeyError:
                allVariablesFound = True;


        if variableId == 0: 
            warnings.warn("No variables found in file %d" % fileNo, UserWarning);


        solutionId += 1;
   
    return [solutionsA, solutionsB] ;

def getVariables(solution_id, cursor):
    results = cursor.execute("SELECT solution_id, variable_id, variable FROM variables WHERE solution_id = {0} ORDER BY variable_id".format(solution_id)) 
    results = results.fetchall(); 
    variables = [];
    for variable in results:
        variables.append(
            {'solutionId'  :   int(variable[0]),
             'variableId' :   int(variable[1]),
             'variable'   :   float(variable[2])
                })

    return variables;

def getObjectives(solution_id, cursor):
    results = cursor.execute("SELECT solution_id, objective_id, objective FROM objectives WHERE solution_id = {0} ORDER BY objective_id".format(solution_id));
    results = results.fetchall();
    objectives = [];
    for objective in results:
        objectives.append(
                {'solutionId'  :   int(objective[0]),
                 'objectiveId' :   int(objective[1]),
                 'objective'   :   float(objective[2])
                    })

    return objectives;

def getDates(cursor, solutionId):
    results = cursor.execute("SELECT solution_id, date_id, date FROM dates WHERE solution_id = {0} ORDER BY date_id".format(solutionId));
    results = results.fetchall();
    dates = [] 
    for date in results:
        dates.append({
            'solutionId'  : date[0],
            'dateId' : date[1],
            'date' : date[2],
            })

    return dates;

def getGrowth(cursor,solutionId):
    results = cursor.execute("SELECT solution_id, growth_day, growth_amount FROM growth WHERE solution_id = {0} ORDER BY growth_day".format(solutionId));
    results = results.fetchall();
    growthDates = []
    for growthDate in results:
        growthDates.append(
                {'solutionId': growthDate[0],
                 'growthDate' : growthDate[1],
                 'growthAmount' : growthDate[2]

        })

    return growthDates;

def getSolution(cursor, solutionId):
    result = cursor.execute("SELECT solution_id, file_no, generation_id FROM solutions WHERE solution_id = {0}".format(solutionId));
    result = result.fetchall()[0];
    solution = {'solutionId': int(result[0]), 'fileNo' : int(result[1]), 'genId' : int(result[2])}

    return solution;

#def getVariable(cursor, solutionId):
#    result = cursor.execute("SELECT");

def deleteSolutions(solutionsToDelete, cursor):
    # Delete from solutions
    deleteQuery = 'DELETE FROM solutions WHERE solution_id in (';
    deleteQuery += ",".join(str(item) for item in solutionsToDelete)
    deleteQuery += ');';

    cursor.execute(deleteQuery);

    # Delete from objectives
    deleteQuery = 'DELETE FROM objectives WHERE solution_id in (';
    deleteQuery += ",".join(str(item) for item in solutionsToDelete)
    deleteQuery += ');';

    cursor.execute(deleteQuery);


    # Delete from growth
    deleteQuery = 'DELETE FROM growth WHERE solution_id in (';
    deleteQuery += ",".join(str(item) for item in solutionsToDelete)
    deleteQuery += ');';

    cursor.execute(deleteQuery);


    # Delete from variables
    deleteQuery = 'DELETE FROM variables WHERE solution_id in (';
    deleteQuery += ",".join(str(item) for item in solutionsToDelete)
    deleteQuery += ');';

    cursor.execute(deleteQuery);

    # Delete from dates
    deleteQuery = 'DELETE FROM dates WHERE solution_id in (';
    deleteQuery += ",".join(str(item) for item in solutionsToDelete)
    deleteQuery += ');';

    cursor.execute(deleteQuery);

def findDomdSolutions(cursor, solutionRange1 = None, solutionRange2 = None):

    # Get a list of solutions
    if solutionRange2 is None:
        results = cursor.execute("SELECT solution_id FROM solutions");
        solutionRange1 = results.fetchall(); 
        solutionRange1 = list((item[0] for item in solutionRange1));

    if solutionRange2 is None:
        results = cursor.execute("SELECT solution_id FROM solutions");
        solutionRange2 = results.fetchall(); 
        solutionRange2 = list((item[0] for item in solutionRange2));

    solCount = len(list(set().union(solutionRange1, solutionRange2)));

    print("%d solutions coming in." % solCount);
    dominatedSolutions = [];
    ind = 0;
    currentPer = -1;

    # Iterate through the solutiosn
    for solution in solutionRange1:
        if ((ind/solCount) * 100) != currentPer:
            currentPer = ((ind/solCount) * 100);
            sys.stdout.write("\rFinding dominated solutions. %d%% Completed" % currentPer); 

        objectives = getObjectives(solution,cursor);

        if solution in dominatedSolutions:
            ind += 1;
            continue

        for competingSolution in solutionRange2:

            if competingSolution in dominatedSolutions:
                continue

            if solution == competingSolution:
                continue
        
            competingObjectives = getObjectives(competingSolution, cursor);

            result = isDominated(objectives, competingObjectives);         

            if result[1]:
                dominatedSolutions.append(competingSolution);

            if result[0]:
                dominatedSolutions.append(solution);    
                break;

        ind += 1;

    sys.stdout.write("\rFinding dominated solutions. %d%% Completed\n" % 100); 
    print("Completed. %d solutions in, %d non dominated solutions found." % (solCount, len(dominatedSolutions)));

    return dominatedSolutions;



def prettyPrint(cur):
    
    results = cur.execute("SELECT solution_id FROM solutions");
    solutionIds = results.fetchall(); 

    sys.stdout.write("bar = [");
    # Iterate through the solutiosn
    for solution in solutionIds:
        sys.stdout.write(",".join((str(item['objective']) for item in getObjectives(solution[0],cur))) + ";");

    sys.stdout.write("]");


def isDominated(solutionA, solutionB):
   
    if len(solutionA) != len(solutionB):
        print("\nNumber of objectives do not match");
        sys.exit(1);


    # Assumee non dominated
    aDominatedByB = True;
    bDominatedByA = True;

    isEqual = True;
    for i in range(0, len(solutionA)):
        if solutionA[i]['objective'] < solutionB[i]['objective']:
            aDominatedByB = False;

        if solutionB[i]['objective'] < solutionA[i]['objective']:
            bDominatedByA = False;
        
        isEqual = isEqual and (solutionB[i]['objective'] == solutionA[i]['objective'])
    if isEqual:
        aDominatedByB = True;
        bDominatedByA = False;

    return [aDominatedByB, bDominatedByA]; 

def addSolution(cursor, solutionId, fileNo, generationId):
        cursor.execute( '''
            INSERT INTO solutions(
                solution_id,
                file_no,
                generation_id
            )
            values (
                {0},
                {1},
                {2}
            );
        '''.format(solutionId, fileNo, generationId))


def addObjective(cursor, solutionId, objectiveId, objective):
    cursor.execute( '''
        INSERT INTO objectives(
            solution_id,
            objective_id,
            objective
        )
        values (
            {0},
            {1},
            {2}
        );

            
    '''.format(
        solutionId,
        objectiveId,
        objective
        ))

def addDate(cursor, solutionId, dateId, date):
    cursor.execute('''
        INSERT INTO dates (
            solution_id,
            date_id,
            date
        )
        values (
            {0},
            {1},
            {2}
        );
       '''.format(solutionId,
           dateId,
           date 
           ) )

def addGrowthDate(cursor, solutionId, growthDate, growthAmount):
    cursor.execute('''
        INSERT INTO growth (
            solution_id,
            growth_day,
            growth_amount
        )
        values (
            {0},
            {1},
            {2}
        );

       '''.format(solutionId, growthDate, growthAmount ))

def addVariable(cursor, solutionId, variableId, variable):
    cursor.execute('''
        INSERT INTO variables (
            solution_id,
            variable_id,
            variable
        )
        values (
            {0},
            {1},
            {2}
        );
       '''.format(solutionId,
           variableId,
           variable
           ))

def getSolutionIds(db):
    # Find the highest id in db
    sqlResults = db.cursor().execute("SELECT solution_id FROM solutions"); 
    solutionIds = sqlResults.fetchall();
    solutionIds = list((item[0] for item in solutionIds)) 

    return solutionIds;

def concatDbA2DbB(dbA, dbB):

    solutionsA = getSolutionIds(dbA); 
    solutionsB = [];

    if len(solutionsA) is 0:
        ind = 0;
    else:
        ind = max(solutionsA) + 1; 

    sqlResults = dbB.cursor().execute("SELECT solution_id, file_no, generation_id FROM solutions");
    solutions = sqlResults.fetchall(); 
   
    for solution in solutions:
        solutionsB.append(ind);
        addSolution(dbA.cursor(), ind, solution[1], solution[2]); 

        # Add objectives
        for objective in getObjectives(solution[0],dbB.cursor()):
            addObjective(dbA.cursor(), ind, objective['objectiveId'], objective['objective']);

        for variable in getVariables(solution[0], dbB.cursor()):
            addVariable(dbA.cursor(), ind, variable['variableId'], variable['variable']);

        # Add dates
        for date in getDates(dbB.cursor(), solution[0]):
            addDate(dbA.cursor(),ind, date['dateId'], date['date'])

        # Add growth
        for growthDate in getGrowth(dbB.cursor(),solution[0]):
            addGrowthDate(dbA.cursor(), ind, growthDate['growthDate'], growthDate['growthAmount'] ) 


        ind += 1;
    
    dbA.commit(); 

    dbB.close();

    return [solutionsA, solutionsB];







