import sys, glob;
from subprocess import call;

rawFiles = sys.argv[1:]
fileList = [];

top=10
solsPerGen=100

# Make sure we expand all * in the files
for globFiles in rawFiles: 
        for file in glob.glob(globFiles):
                fileList.append(file);
ind = 0;

for file in fileList:
    id = str(ind).zfill(5);
    solCount = top*solsPerGen;
    print("Processing %s" % file)
    f = open("top%dgen-%s.csv" % (top, id), 'w');
    call(["head", "-n" "1",file ], stdout=f)
    call(["tail", "-n", str(solCount),file ], stdout=f)
    ind += 1; 

