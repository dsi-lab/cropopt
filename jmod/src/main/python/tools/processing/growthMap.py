import parseNSGAOut, sqlite3, sys;

# This script takes the date columns and the growth columns and builds a new 
# CSV table containing a column for each day in the growing season, and the 
# growth amount on each day. 

dbFile = sys.argv[1];
champDB = sqlite3.connect(dbFile);

noOfGrowthDate = champDB.execute("select count(distinct growth_day) from growth");
noOfGrowthDate = noOfGrowthDate.fetchall()[0][0];

minGrowthDate = champDB.execute("select min(distinct growth_day) from growth");
minGrowthDate = minGrowthDate.fetchall()[0][0];

noOfObjectives = champDB.execute("select count(distinct objective_id) from objectives");
noOfObjectives =  noOfObjectives.fetchall()[0][0];

solutions = champDB.execute("select solution_id from solutions");
solutions = list(( tempSol[0]  for tempSol in solutions.fetchall()))

growthDays = range(minGrowthDate, minGrowthDate + noOfGrowthDate);

print("solution_id, " +  ','.join(("objective_" + str(obj) for obj in range(0,noOfObjectives))) + ',' +  ','.join(("day_" + str(day) for day in growthDays)));

for solution in solutions:

    # Get objectives 
    objectives = parseNSGAOut.getObjectives(solution, champDB.cursor());

    # Get days
    days = parseNSGAOut.getDates(champDB.cursor(), solution) 

    # Get variables
    variables = parseNSGAOut.getVariables(solution, champDB.cursor());

    # Build map
    growth = []; 

    growthAmounts = parseNSGAOut.getGrowth(champDB.cursor(), solution);

    for dateInd in range(0, noOfGrowthDate): 
        growth.append(str(growthAmounts[dateInd]['growthAmount']));



    sys.stdout.write("%d," % solution + ','.join((str(obj['objective']) for obj in objectives)) + ",") 

    sys.stdout.write(','.join(growth));

    sys.stdout.write("\n");



