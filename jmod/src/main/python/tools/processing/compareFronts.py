import EMOTools, sys, re, sqlite3, csv;

files = sys.argv[1:];

if len(files) != 2:
    print("Only two fronts permitted");
    sys.exit(1);


# Start -- Check what kind of files we have
if re.match(".*\.csv$", files[0]):
    file1IsCSV = True;
elif re.match(".*\.db$", files[0]):
    file1IsCSV = False
else:
    print("Unrecognized file format %s" % files[0]);
    sys.exit(1);


if re.match(".*\.csv$", files[1]):
    file2IsCSV = True;
elif re.match(".*\.db$", files[1]):
    file2IsCSV = False
else:
    print("Unrecognized file format %s" % files[1]);
    sys.exit(1);
# End -- Check what kind of files we have


db = sqlite3.connect(':memory:')

EMOTools.init_db(db)

print("Populating database...");

# Start -- Create a single database consisting of both Pareto fronts

# Ids from original files
file1SolIds = [];
file2SolIds = [];

# Arrays that keep track of which index within the combined database belongs to 
#    which front
file1SolCombinedIndices = [];
file2SolCombinedIndices = [];


# Both CSV files
if file1IsCSV and file2IsCSV:
    with open(files[0], 'r') as csvfile:
        csvreader = csv.DictReader(csvfile);
        EMOTools.concatDbA2CsvB(db,csvreader, 0);

    file1SolIds = EMOTools.getCsvSolutionIds(files[0]);

    # Find the solutions from file 1
    file1SolCombinedIndices = EMOTools.getSolutionIds(db);

    with open(files[1], 'r') as csvfile:
        csvreader = csv.DictReader(csvfile);
        solutions = EMOTools.concatDbA2CsvB(db,csvreader, 1);

    file2SolIds = EMOTools.getCsvSolutionIds(files[1]);

    # Find the solutions from file 2
    file2SolCombinedIndices = EMOTools.getSolutionIds(db);
    for file1Sol in file1SolCombinedIndices:
        file2SolCombinedIndices.remove(file1Sol);

# CSV file and DB file
elif file1IsCSV and not file2IsCSV:

    with open(files[0], 'r') as csvfile:
        csvreader = csv.DictReader(csvfile);
        EMOTools.concatDbA2CsvB(db,csvreader, 0);
        file1SolIds = EMOTools.getCsvSolutionIds(files[0]);

    # Find the solutions from file 1
    file1SolCombinedIndices = EMOTools.getSolutionIds(db);

    tempDb = sqlite3.connect(files[1])
    file2SolIds = EMOTools.getSolutionIds(tempDb);
    solutions = EMOTools.concatDbA2DbB(db, tempDb);

    # Find the solutions from file 2
    file2SolCombinedIndices = EMOTools.getSolutionIds(db);
    for file1Sol in file1SolCombinedIndices:
        file2SolCombinedIndices.remove(file1Sol);

# DB file and CSV file
elif not file1IsCSV and file2IsCSV:
    with open(files[1], 'r') as csvfile:
        csvreader = csv.DictReader(csvfile);
        EMOTools.concatDbA2CsvB(db,csvreader, 1);

    file2SolIds = EMOTools.getCsvSolutionIds(files[1]); 

    # Find the solutions from file 2
    file2SolCombinedIndices = EMOTools.getSolutionIds(db);

    tempDb = sqlite3.connect(files[0])
    file1SolIds = getSolutionIds(tempDb);
    solutions = EMOTools.concatDbA2DbB(db, tempDb);

    # Find the solutions from file 1
    file1SolCombinedIndices = EMOTools.getSolutionIds(db);
    for file2Sol in file2SolCombinedIndices:
        file1SolCombinedIndices.remove(file2Sol);

# DB file and DB file
elif not file1IsCSV and not file2IsCSV:

    tempDb = sqlite3.connect(files[0])
    file1SolIds = EMOTools.getSolutionIds(tempDb);
    EMOTools.concatDbA2DbB(db, tempDb);
    # Find the solutions from file 1
    file1SolCombinedIndices = EMOTools.getSolutionIds(tempDb);

    tempDb = sqlite3.connect(files[1])
    file2SolIds = EMOTools.getSolutionIds(tempDb);
    solutions = EMOTools.concatDbA2DbB(champ, tempDb);

    # Find the solutions from file 2 
    file2SolCombinedIndices = EMOTools.getSolutionIds(db);
    for file1Sol in file1SolCombinedIndices:
        file2SolCombinedIndices.remove(file1Sol);

# End -- Create a single database consisting of both Pareto fronts

# Start -- Find dominated solutions

print("Looking for dominated solutions...");

# dominated sols is an array of dominated solutions 
dominatedSols = EMOTools.findDomdSolutions(db.cursor(), solutions[0], solutions[1])
print("Done.");

# these are the ids from file 1 and 2 with ids from the combined database
file1Domd = [];
file2Domd = [];

for dominatedSol in dominatedSols: 
    if dominatedSol in file1SolCombinedIndices:
        file1Domd.append(dominatedSol);
    elif dominatedSol in file2SolCombinedIndices:
        file2Domd.append(dominatedSol);

originalSols1 = []
for file1DomdItem in file1Domd:
    originalSols1.append(file1SolIds[file1SolCombinedIndices.index(file1DomdItem)])    

originalSols2 = []
for file2DomdItem in file2Domd:
    originalSols2.append(file2SolIds[file2SolCombinedIndices.index(file2DomdItem)])    


# End -- Find dominated solutions

# Start -- Report findings

print("%d%% of solutions (%d out of %d) from file 1 are dominated by file 2." 
        % (((len(file1Domd) / len(file1SolCombinedIndices))*100), len(file1Domd), len(file1SolCombinedIndices)));

print("%d%% of solutions (%d out of %d) from file 2 are dominated by file 1." 
        % (((len(file2Domd) / len(file2SolCombinedIndices))*100), len(file2Domd), len(file2SolCombinedIndices)));

print("dominated solutions from file 1")
originalSols1.sort();
print(originalSols1)

print("dominated solutions from file 2")
originalSols2.sort();
print(originalSols2)

# End -- Report findings
















