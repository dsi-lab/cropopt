import EMOTools, sys, glob, sqlite3, io, time, datetime, traceback, re, csv;

# Takes a list of databases or CSV files of multi-objective algorithm ouputs, 
# combines them, and then deletes the dominated solutions. 

rawFiles = sys.argv[1:]
fileList = [];

# Make sure we expand all * in the files
for globFiles in rawFiles: 
    deglobedFiles = glob.glob(globFiles);
    if len(deglobedFiles) == 0:
        print("File %s not found." % globFiles);
        sys.exit(1);

    for file in deglobedFiles:
        fileList.append(file);

champDB = sqlite3.connect(':memory:')

EMOTools.init_db(champDB)

try:

    for contenderFile in fileList:

        print("Loading next file " + contenderFile);

        if re.match(".*\.csv$", contenderFile): 
            with open(contenderFile, 'r') as csvfile:
                csvreader = csv.DictReader(csvfile);
                solutions = EMOTools.concatDbA2CsvB(champDB, csvreader, int(re.search("-\d+", contenderFile).group()[1:]) );

        elif re.match(".*\.db$", contenderFile):
            contenderDB = sqlite3.connect(contenderFile);
            solutions = EMOTools.concatDbA2DbB(champDB, contenderDB)
        else:
            print("File format not recognized %s" % contenderFile);
            sys.exit(1);
        
        solutionsToDelete = EMOTools.findDomdSolutions(champDB.cursor(), solutions[1], solutions[1] + solutions[0]);

        print("Deleting dominated solutions");

        EMOTools.deleteSolutions(solutionsToDelete, champDB.cursor());

        success=True;

    print("done");

except (KeyboardInterrupt, Exception) as e:

    print("Interupted. Exporting work in progress");

    success=False;

    EMOTools.deleteSolutions(solutions[1], champDB.cursor());

    print(traceback.format_exc())

print("Exporting file...")

#if success:
#    EMOTools.prettyPrint(champDB.cursor());

tempfile = io.StringIO();

for line in champDB.iterdump():
    tempfile.write('%s\n' % line);

champDB.close();

tempfile.seek(0);

timeStamp = time.time();

formattedStamp =  datetime.datetime.fromtimestamp(timeStamp).strftime('%Y-%m-%d_%H-%M-%S_nondoms.db');

champDB = sqlite3.connect("C:\\Users\\kroppian\\Desktop\\%s" % formattedStamp)

champDB.cursor().executescript(tempfile.read());

champDB.commit();

champDB.close();


