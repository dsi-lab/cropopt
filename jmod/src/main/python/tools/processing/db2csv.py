import parseNSGAOut, sqlite3, sys;

# Dumps the whole database into a CSV file

dbFile = sys.argv[1];

champDB = sqlite3.connect(dbFile);

solutions = champDB.execute("select solution_id from solutions");
solutions = list(( tempSol[0]  for tempSol in solutions.fetchall()))

noOfObjectives = champDB.execute("select count(distinct objective_id) from objectives");
noOfObjectives =  noOfObjectives.fetchall()[0][0];

noOfDates = champDB.execute("select count(distinct date_id) from dates");
noOfDates =  noOfDates.fetchall()[0][0];

noOfVariables = champDB.execute("select count(distinct variable_id) from variables");
noOfVariables = noOfVariables.fetchall()[0][0];

noOfGrowthDate = champDB.execute("select count(distinct growth_day) from growth");
noOfGrowthDate = noOfGrowthDate.fetchall()[0][0];

minGrowthDate = champDB.execute("select min(distinct growth_day) from growth");
minGrowthDate = minGrowthDate.fetchall()[0][0];


header = ["solution_id", "file_no", "gen_id"] + \
            list(("objective_%d" % obj for obj in range(0,noOfObjectives))) + \
            list(("variable_%d" % var for var in range(0,noOfVariables))) + \
            list(("day_%d" % day for day in range(0,noOfDates))) + \
            list(("growth_date_%d" % obj for obj in range(minGrowthDate, minGrowthDate + noOfGrowthDate))) 
            



print(','.join(header) + ',')
for solution in solutions:
    sys.stdout.write("%d, " % solution);

    solutionInfo = parseNSGAOut.getSolution(champDB.cursor(), solution)    

    sys.stdout.write("%d, %d" % (solutionInfo['fileNo'], solutionInfo['genId']) + ",");
    sys.stdout.write(",".join((str(obj['objective']) for obj in parseNSGAOut.getObjectives(solution, champDB.cursor()))) + ',');
    sys.stdout.write(",".join((str(var['variable']) for var in parseNSGAOut.getVariables(solution, champDB.cursor()))) + ',');
    sys.stdout.write(",".join((str(day['date']) for day in parseNSGAOut.getDates(champDB.cursor(), solution))) + ',');
    sys.stdout.write(",".join((str(growthDay['growthAmount']) for growthDay in parseNSGAOut.getGrowth(champDB.cursor(), solution))) + ',');
    sys.stdout.write("\n");




