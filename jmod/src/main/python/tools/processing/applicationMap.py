import parseNSGAOut, sqlite3, sys;

# This script takes the date columns and the variable columns and builds a new 
# CSV table containing a column for each day in the growing season, and the 
# application amount on each day. Only designed for single application amount 
# at this point

dbFile = sys.argv[1];
champDB = sqlite3.connect(dbFile);

noOfGrowthDate = champDB.execute("select count(distinct growth_day) from growth");
noOfGrowthDate = noOfGrowthDate.fetchall()[0][0];

minGrowthDate = champDB.execute("select min(distinct growth_day) from growth");
minGrowthDate = minGrowthDate.fetchall()[0][0];

noOfObjectives = champDB.execute("select count(distinct objective_id) from objectives");
noOfObjectives =  noOfObjectives.fetchall()[0][0];

solutions = champDB.execute("select solution_id from solutions");
solutions = list(( tempSol[0]  for tempSol in solutions.fetchall()))

growthDays = range(minGrowthDate, minGrowthDate + noOfGrowthDate);

print("solution_id, " +  ','.join(("objective_" + str(obj) for obj in range(0,noOfObjectives))) +  ',solution#,' +  ','.join(("day_" + str(day) for day in growthDays)));

for solution in solutions:

    # Get objectives 
    objectives = parseNSGAOut.getObjectives(solution, champDB.cursor());

    # Get days
    days = parseNSGAOut.getDates(champDB.cursor(), solution) 

    # Get variables
    variables = parseNSGAOut.getVariables(solution, champDB.cursor());

    # Build map
    applications = []; 

    ind = 0;
    for applicationDate in  days:
        applications.append({'day': applicationDate['date'],'amount': variables[ind]['variable'] });
        ind += 1;

    applicationAmounts = [];

    for date in growthDays: 
        application = [x for x in applications if x['day'] == date];
        if len(application) != 0:
            applicationAmounts.append(str(round(application[0]['amount']))); 
        else:
            applicationAmounts.append("0");

    sys.stdout.write("%d," % solution + ','.join((str(obj['objective']) for obj in objectives)) + ",") 

    sys.stdout.write("%d," % len(list(filter(lambda x: x != 0, [int(x) for x in applicationAmounts])))  );

    sys.stdout.write(','.join(applicationAmounts));

    sys.stdout.write("\n");



