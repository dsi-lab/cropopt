import numpy as np
import csv;

a = np.arange(15).reshape(3, 5)

def parseObjFile(fileName):
    result = np.arange(0);
    with open(fileName, 'rt') as csvFile:
        reader = csv.reader(csvFile, delimiter=' ',quotechar='|')
        rowcounter = 0;
        for row in reader: 
            rowInFloat = []
            for cell in row:
                rowInFloat.append(float(cell));

            if result.size is 0:
                result = np.array(rowInFloat)
            else:
                result = np.vstack([result, rowInFloat])

            rowcounter += 1;
    
    return result;


def parseObjFiles(fileNames):
    result = np.arange(0);
    for file in fileNames: 
        fileArr = parseObjFile(file);

        if result.size == 0:
            result = fileArr
        else:
            result = np.vstack([result, fileArr])

    return result



