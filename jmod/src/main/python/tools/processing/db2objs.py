import EMOTools, sqlite3, sys;

# Small script to just print out the objectives from the databas

dbFile = sys.argv[1];

champDB = sqlite3.connect(dbFile);

solutions = champDB.execute("select solution_id from solutions");
solutions = list(( tempSol[0]  for tempSol in solutions.fetchall()))

noOfObjectives = champDB.execute("select count(distinct objective_id) from objectives");
noOfObjectives =  noOfObjectives.fetchall()[0][0];

noOfDates = champDB.execute("select count(distinct date_id) from dates");
noOfDates =  noOfDates.fetchall()[0][0];

noOfVariables = champDB.execute("select count(distinct variable_id) from variables");
noOfVariables = noOfVariables.fetchall()[0][0];

noOfGrowthDate = champDB.execute("select count(distinct growth_day) from growth");
noOfGrowthDate = noOfGrowthDate.fetchall()[0][0];

minGrowthDate = champDB.execute("select min(distinct growth_day) from growth");
minGrowthDate = minGrowthDate.fetchall()[0][0];


header = ["solution_id", "file_no", "gen_id"] + \
            list(("objective_%d" % obj for obj in range(0,noOfObjectives))) 
            



print(','.join(header))
for solution in solutions:
    sys.stdout.write("%d, " % solution);

    solutionInfo = EMOTools.getSolution(champDB.cursor(), solution)    

    sys.stdout.write("%d, %d" % (solutionInfo['fileNo'], solutionInfo['genId']) + ",");
    sys.stdout.write(",".join((str(obj['objective']) for obj in EMOTools.getObjectives(solution, champDB.cursor()))) + ',');
    sys.stdout.write("\n");




