% Script for preparing input file to compute hypervolume indicator
clc, clear, close all
opt = 2; % 1(NSE); 2(RMSE)

switch opt
    case 1
        inputDIR = './NSE';
        BigFile = 'Pareto_gen_Opt1.txt';
    case 2
        inputDIR = './RMSE';
        BigFile = 'Pareto_gen_Opt2.txt';
end

fileList = struct2table(dir(fullfile(inputDIR,'*obj*.dat')));
f = cd;
cd(inputDIR)
npar = size(fileList,1);
system(['echo #> ../',BigFile]);
h = waitbar(0,'Please wait...');
for i=1:npar
    filename1 = cell2mat(table2cell(fileList(i,1)));
    system(['type ',filename1,' >> ../',BigFile]);
    system(['echo #>> ../',BigFile]); 
    waitbar(i/npar)
end
close(h)
cd(f)