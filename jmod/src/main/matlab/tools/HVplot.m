% Hypervolume results
clc, clear, close all
HV1 = load('HV1.dat');
HV2 = load('HV2.dat');

% Normalization
HV1 = (HV1-min(HV1))/(max(HV1)-min(HV1));
HV2 = (HV2-min(HV2))/(max(HV2)-min(HV2));
figure
hold on
plot(HV1,'r--','linewidth',2)
xlim([0, Inf])
plot(HV2,'b-','linewidth',2)
xlim([0, Inf])
ylim([-Inf, 1.1])
xlabel('Generation number')
ylabel('Normalized hypervolume indicator')
legend('NSE strategy','RMSE strategy','location','SE')
box on