function [graph, cbar] = scatter5(objectives, cmap)

    if size(objectives,2) ~= 5
        error("Data, and thus input matrix, must be 4 dimensional " + ...
            "and thus have four columns");
    end
   
    %% Gather min/max data
    % fourth objective
    fourthObj = objectives(:,4);
    minFourthObj = min(fourthObj);
    maxFourthObj = max(fourthObj);
    
    % Fifth objective
    fifthObj = objectives(:,5);
    minFifthObj = min(fifthObj);
    maxFifthObj = max(fifthObj);
    
    %% Normalize data 
    % Normalize the fourth objecitve
    fourthObj = fourthObj - minFourthObj;
    fourthObj = fourthObj / max(fourthObj);
    
    % Normalize the fifth objective 
    fifthObj = fifthObj - minFifthObj;
    fifthObj = fifthObj / max(fifthObj);
        
    
    %% Construct the color map for the graph points
    % Find where the values stand on the colormap scale (1 to 64)
    colorsIndices = round(fourthObj * 63) + 1;
    
    colors = cmap(colorsIndices,:);
    
    %% Construct the fifth dimension marker size scale
    
    
    
    %% Plot 
    colormap(cmap);
    graph = scatter3(objectives(:,1), objectives(:,2), objectives(:,3), ones(100,1)*50, colors,'filled');
    
    %% Build the color bar 
    ticks = num2cell(round(minFourthObj:(maxFourthObj/6):maxFourthObj));
    cbar = colorbar('TickLabels', ticks);
%     cbar = colorbar;
%     cbar.Limits = [0 maxFourthObj];
end
