function cleanData(directory)

    dailyIrrMax = 50;
    dailyNitroMax = 150;
    dailyPhosMax = 10;

    irrigationApps = 16;
    nitrogenApps = 0;
    phosApps = 0;
    objNum = 0;
    
    gen_count = 5;

    if (nargin<1 || isempty(directory))
        directory = 'C:\Users\kroppian\Projects\output\cropModeling\javaNSGAIII\2017-07-13-1319-dtlz1_3obj-000-000-P0092-G0150\unified_nsga3\run000\';
    end
    
    obj_f = directory + "\obj_gen_" + sprintf('%04d',gen_count - 1) + ".dat";
    
    obj = dlmread(obj_f, ' ');
    
    var_f =  directory + "\var_gen_" + sprintf('%04d',gen_count - 1) + ".dat";
    
    vars = dlmread(var_f, ' ');
    
    %cleaned = [-obj(:,1), obj(:,2)*dailyIrrMax, obj(:,3)*dailyNitroMax, obj(:,4)*dailyPhosMax];
    %cleaned = [-obj(:,1), obj(:,2)];
    cleaned = [-obj(:,1)];


        
    for n = 1:irrigationApps
        cleaned = [cleaned (vars(:,n) * dailyIrrMax)];
    end
    
%     for n = (irrigationApps + 1):(irrigationApps + nitrogenApps)
%         cleaned = [cleaned (vars(:,n)  * dailyNitroMax)];
%     end
% 
%     for n = (irrigationApps + nitrogenApps + 1):(irrigationApps + nitrogenApps + phosApps)
%         cleaned = [cleaned (vars(:,n)  * dailyPhosMax)];
%     end
    
    fprintf("TotalYield,");
    for m = 1:irrigationApps
        fprintf(",");
    end
    fprintf("TotalIrrigation,");
    for m = 1:nitrogenApps
        fprintf(",");
    end
    fprintf("TotalNitrogen,");
    for m = 1:phosApps
        fprintf(",");
    end
    fprintf("TotalPhosphorus,\n");
    
    for n = 1:size(cleaned,1)
%         for m = 1:size(cleaned,2)
%                 fprintf("%d,", int16(cleaned(n,m)));
%         end
%         fprintf("\n");

        fprintf("%d,", int16(cleaned(n,1)));
        for m = (objNum + 1):(irrigationApps + objNum)
            fprintf("%d,", int16(round(cleaned(n,m))));
        end
        
%         fprintf("%d,", int16(cleaned(n,2)));
%         for m = (irrigationApps + objNum + 1):(irrigationApps + objNum + nitrogenApps)
%             fprintf("%d,", int16(round(cleaned(n,m))));
%         end
%         
%         fprintf("%d,", int16(cleaned(n,3)));
%         for m = (irrigationApps + objNum + nitrogenApps + 1):(irrigationApps + objNum + nitrogenApps + phosApps)
%             fprintf("%d,", int16(round(cleaned(n,m))));
%         end
        
        fprintf("%d,\n", int16(cleaned(n,4)));
    end
end
