function [graph, cbar] = scatter4(objectives, cmap)

    if size(objectives,2) ~= 4
        error("Data, and thus input matrix, must be 4 dimensional " + ...
            "and thus have four columns");
    end
        

    fourthObj = objectives(:,4);
    
    minFourthObj = min(fourthObj);
    maxFourthObj = max(fourthObj);
    
    
    %% Construct the color map for the graph points
    % Normalize the fourth objecitve
    fourthObj = fourthObj - minFourthObj;
    fourthObj = fourthObj / max(fourthObj);
    
    
%     fourthObj = fourthObj / maxFourthObj;
    
    % Find where the values stand on the colormap scale (1 to 64)
    colorsIndices = round(fourthObj * 63) + 1;
    
    colors = cmap(colorsIndices,:);
    
    %% Plot 
    colormap(cmap);
    graph = scatter3(objectives(:,1), objectives(:,2), objectives(:,3), ones(100,1)*50, colors,'filled');
    
    %% Build the color bar 
    ticks = num2cell(round(minFourthObj:(maxFourthObj/6):maxFourthObj));
    cbar = colorbar('TickLabels', ticks);
%     cbar = colorbar;
%     cbar.Limits = [0 maxFourthObj];
end
