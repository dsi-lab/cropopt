function [freqMat] = get_frequency(variables)
    %GET_FREQUENCY calculates frequency of application
    %   Given j variables for n solutions, calculates how many applications
    %   were applied for each solution.
    freqMat = zeros(size(variables,2),1);

    for n=1:size(variables,2)
        freqMat(n,1) = size(find(variables(:,n) ~= 0),1);
    end

end

