function [objectives] = get_objectives(tab)
    %GET_OBJECTIVES Returns and formats objectives
    %   Given a table of results, returns j objectives for n solutions

    objcols = find(cellfun('length',regexp(tab.Properties.VariableNames,'objective')) == 1);

    objectives = zeros(height(tab), size(objcols,2));

    for n=1:size(objcols,2)
        objectives(:,n) = table2array(tab(:,['objective_', int2str(n - 1)]));
    end

end

