function [variables] = get_variables(tab)
    %GET_VARIABLES Returns and formats variables
    %   Given a table of results, returns j variables for n solutions

    objcols = find(cellfun('length',regexp(tab.Properties.VariableNames,'variable')) == 1);

    variables = zeros(height(tab), size(objcols,2));

    for n=1:size(objcols,2)
        % Construct the variable name to pull from the table 
        variableName = ['variable_', int2str(n - 1)];
        
        % Pull that variable name out of the table and stick concatenate
        % it to the variable matrix. Also round the values to their integer
        % value
        variables(:,n) = round(table2array(tab(:,variableName)));
        
    end

    

end

