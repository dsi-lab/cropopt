function toDate(date)

dateList = strsplit(date,{',',' ','\t'},'CollapseDelimiters', true);
for i = 1:length(dateList)
    date = dateList(i);
    
    year = extractBefore(date,3);
    days = extractAfter(date,2);
    
    if ("58" < year) && ("99" > year)
        year = "19" + year;
    else
        year = "20" + year;
    end
    
    leapYears = ["1960", "1964", "1968", "1972", "1976", "1980", "1984", "1988", "1992", "1996", "2000", "2004", "2008", "2012", "2016", "2020", "2024", "2028", "2032", "2036", "2040", "2044", "2048", "2052", "2056"];
    leap = find(leapYears == year); % Determine if leap year
    
    % Convert days from string to number
    days = char(days);
    days = str2num(days);
    
    % Adjust if leap year
    if (leap >= 1)
        if days >= 59
            days = days - 1;
        end
    end
    
    % January
    if(1 < days) && (31 >= days)
        days = num2str(days);
        if length(days) == 1
            days = "0" + days;
        end
        disp(year + "-" + "01" + "-" + days);
      
    % February
    elseif(32 <= days) && (59 >= days)
        days = days - 31;
        days = num2str(days);
        if length(days) == 1
            days = "0" + days;
        end
        disp(year + "-" + "02" + "-" + days);
    
    % March
    elseif(60 <= days) && (90 >= days)
        days = days - 59;
        days = num2str(days);
        if length(days) == 1
            days = "0" + days;
        end
        disp(year + "-" + "03" + "-" + days);
    
    % April
    elseif(91 <= days) && (120 >= days)
        days = days - 90;
        days = num2str(days);
        if length(days) == 1
            days = "0" + days;
        end
        disp(year + "-" + "04" + "-" + days);
     
    % May
    elseif(121 <= days) && (151 >= days)
        days = days - 120;
        days = num2str(days);
        if length(days) == 1
            days = "0" + days;
        end
        disp(year + "-" + "05" + "-" + days);
  
    % June
    elseif(152 <= days) && (181 >= days)
        days = days - 151;
        days = num2str(days);
        if length(days) == 1
            days = "0" + days;
        end
        disp(year + "-" + "06" + "-" + days);
   
    % July
    elseif(182 <= days) && (212 >= days)
        days = days - 181;
        days = num2str(days);
        if length(days) == 1
            days = "0" + days;
        end
        disp(year + "-" + "07" + "-" + days);
 
    % August
    elseif(213 <= days) && (243 >= days)
        days = days - 212;
        days = num2str(days);
        if length(days) == 1
            days = "0" + days;
        end
        disp(year + "-" + "08" + "-" + days);
      
    % September
    elseif(244 <= days) && (273 >= days)
        days = days - 243;
        days = num2str(days);
        if length(days) == 1
            days = "0" + days;
        end
        disp(year + "-" + "09" + "-" + days);
     
    % October
    elseif(274 <= days) && (304 >= days)
        days = days - 273;
        days = num2str(days);
        if length(days) == 1
            days = "0" + days;
        end
        disp(year + "-" + "10" + "-" + days);
  
    % November
    elseif(305 <= days) && (334 >= days)
        days = days - 304;
        days = num2str(days);
        if length(days) == 1
            days = "0" + days;
        end
        disp(year + "-" + "11" + "-" + days);
 
    % December
    else
        days = days - 334;
        days = num2str(days);
        if length(days) == 1
            days = "0" + days;
        end
        disp(year + "-" + "12" + "-" + days);
    end
    
end

end
        
    