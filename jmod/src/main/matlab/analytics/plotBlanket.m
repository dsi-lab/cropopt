function plotBlanket(file, order, labels)

    M = dlmread(file, ' ');
    
    if exist('order','var')
        x = M(:,order(1));
        y = M(:,order(2));
        z = M(:,order(3));
        v = -M(:,order(4));
    else
        x = M(:,1);
        y = M(:,2);
        z = M(:,3);
        v = -M(:,4);
    end
    

    
    minx = min(x);
    miny = min(y);
    minz = min(z);
    minv = min(v);

    maxx = max(x);
    maxy = max(y);
    maxz = max(z);
    maxv = max(v);
    
    sf = fit([x,y],z, 'lowess');
        
    xRange = maxx - minx;
    yRange = maxy - miny;
    
    % Change this to alter the resolution 
    resolution = 100;
    
    xAbsoluteRes = xRange / resolution;
    yAbsoluteRes = yRange / resolution;
    
    [XX,YY] = meshgrid(minx:xAbsoluteRes:maxx, miny:yAbsoluteRes:maxy);

    ZZ = sf(XX,YY);
    
    VV = griddata(x,y,z,v,XX,YY,ZZ);
    
    l = linspace(minv, maxv, 65);
    l = l(2:end-1);

    mask = ~isnan(VV);
    tmpColorMapIndex = imquantize(VV(mask),l);
    VVColorMap = NaN(size(VV));
    VVColorMap(mask) = tmpColorMapIndex;
    clear tmpColorMapIndex mask
    colormap(jet);
    
    
    surf(XX,YY,ZZ,VVColorMap,'EdgeColor','none');
    
    % Pretty surface
    light
    %lighting gouraud
    colormap(flipud(colormap));
    cbar = colorbar('Location','north');
    cbar.TickLabels = arrayfun(@(c) num2str(c),round(l(cbar.Ticks)/500)*500,'UniformOutput',false);
    
    axisFontSize = 22;
    labelFontSize = 32;
    %view(135,50); 
    set(gca, 'FontSize', axisFontSize);
    set(gca, 'YLim', [miny maxy]);
    set(gca, 'XLim', [minx maxx]);
    set(gca, 'ZLim', [minz maxz]);
    %
    % Axis label
    %
    

    if exist('labels','var')
        
        % Color bar label
        title(labels(4), 'FontSize',axisFontSize,'FontWeight', 'normal');
        % Axis labels
        
        xlabel(labels(1), 'FontSize',labelFontSize);
        ylabel(labels(2), 'FontSize',labelFontSize);
        zlabel(labels(3), 'FontSize',labelFontSize);
    end

    
    % Rotate axis labes to be parallel with the axises themselves
    h=get(gca,'xlabel');
    set(h,'rotation',28)
    h=get(gca,'ylabel');
    set(h,'rotation',-27)
    
    % Practical surface
    figure 
    scatter3(x, y, z);
    
    if exist('labels','var')
        xlabel(labels(1));
        ylabel(labels(2));
        zlabel(labels(3));
    end
    
    offset_x = diff(xlim) * .03;
    offset_y = diff(ylim) * .03;
    offset_z = diff(zlim) * .03;
    
    bar = num2str(-1 * v);
    text(x + offset_x, y + offset_y, z + offset_z,bar);
    
end
