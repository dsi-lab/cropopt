function simpleSurf(file, order, labels)

    M = dlmread(file, ' ');
    
    if exist('order','var')
        x = M(:,order(1));
        y = M(:,order(2));
        z = M(:,order(3));
    else
        x = M(:,1);
        y = M(:,2);
        z = M(:,3);
    end
    
    minx = min(x);
    miny = min(y);
    minz = min(z);

    maxx = max(x);
    maxy = max(y);
    maxz = max(z);
    
    sf = fit([x,y],z, 'lowess');
        
    xRange = maxx - minx;
    yRange = maxy - miny;
    
    % Change this to alter the resolution 
    resolution = 100;
    
    xAbsoluteRes = xRange / resolution;
    yAbsoluteRes = yRange / resolution;
    
    [XX,YY] = meshgrid(minx:xAbsoluteRes:maxx, miny:yAbsoluteRes:maxy);
    
    % Figure out the height of each point on the mesh you just created
    ZZ = sf(XX,YY);

    colormap(jet);

    surf(XX,YY,ZZ,'EdgeColor','none');

    
end

    