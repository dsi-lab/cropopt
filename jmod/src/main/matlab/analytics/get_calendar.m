function [calendar] = get_calendar(variables,dates)
    %GET_CALENDAR Builds calendars of applications 
    %   Given j dates and j variables for n rows of solutions, makes n 
    %   rows of application amounts for each day of the year.
    
    maxDate = max(dates(:));

    calendar = zeros(size(variables, 1), maxDate);

    % Initialize array 

    % Go through day 0 to max date and transfer the 
    for solutionId=1:size(variables,1)
        solutionVars = variables(solutionId,:);
        solutionDates = dates(solutionId,:);
        
        % add each variable into its proper date
        for i=1:size(variables,2)
            calendar(solutionId, solutionDates(i)) = solutionVars(i);
        end
        
    end

end

