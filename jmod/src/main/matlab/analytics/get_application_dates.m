function [dates] = get_application_dates(tab)

    objcols = find(cellfun('length',regexp(tab.Properties.VariableNames,'day')) == 1);

    dates = zeros(height(tab), size(objcols,2));

    for n=1:size(objcols,2)
        % Construct the variable name to pull from the table 
        variableName = ['day_', int2str(n - 1)];
        
        % Pull that variable name out of the table and stick concatenate
        % it to the variable matrix. Also round the values to their integer
        % value
        dates(:,n) = round(table2array(tab(:,variableName)));
        
    end

    

end