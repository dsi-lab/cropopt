% load('objectiveData.mat');

csvFile="finalGenIrrNitro.csv";

tab = readtable(csvFile,'Delimiter','comma','ReadVariableNames',true);
objectives = get_objectives(tab);
objectives(:,1) = objectives(:,1)*-1;

colors = {'xr'; 'xb'; 'xg'};

clusterCount = 3;

clusters = kmeans_clust(objectives,clusterCount);

for cluster=1:clusterCount
    scatter3(objectives(clusters==cluster,3), objectives(clusters==cluster,2),objectives(clusters==cluster,1));%,  colors{cluster}); 
    hold on;
end

xlabel('Total Seasonal Irrigation Usage (mm)')
ylabel('Total Seasonal Yield (kg/ha)')
title('Pareto Optimal Solutions Minimizing Irrigation and Maximizing Yield');

