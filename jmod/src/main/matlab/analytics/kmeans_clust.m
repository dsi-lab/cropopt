function [result] = kmeans_clust(data,k)
    

    %% Breaking into three clusters
    noOfObjs = size(data,2);
    normalizedData = zeros(size(data));
    for n=1:noOfObjs
        normalizedData(:,n) = (data(:,n) / max(data(:,n)));
    end
    clusters = kmeans(normalizedData,k,'Replicates', 100);
    
    %% Sorting the clusters by yield
    maxValues = zeros(k,2);

    % Finding the max values in each cluster
    for clst=1:k
        maxValues(clst,:) = [clst,max(data(clusters==clst,1))];
    end
    
    % Sorting the clusters by yield size
    maxValues = sortrows(maxValues,2,'descend');
    
    % Using Matrix magic to rebuild the cluster matrix in the right order
    result = zeros(size(data,1),1);
    for clst=1:k
        result = result + (clusters == maxValues(clst,1)) * clst;
    end
        
end

