import java.util.GregorianCalendar;
import java.util.concurrent.ThreadLocalRandom;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.concurrent.ThreadLocalRandom;
import java.util.Date;
import java.io.IOException;
import org.dsi.ddsat4j.exceptions.FailedRunException;
import org.dsi.ddsat4j.tools.DssatExperiment;
import org.dsi.ddsat4j.exceptions.FileParseException;
import org.dsi.ddsat4j.beans.ResourceData;
import org.dsi.ddsat4j.beans.ResourceDatum;

public class Objective {
  
    public Objective(){}

    public static void main(String[] args){
    
    }

    public static int[] obj(Integer[][] dates, Integer[] x){
        int[] results = new int[2];
  
        results[0] = 0;
        results[1] = 0;
      
        DssatExperiment exp = null;

        try {
            exp = new DssatExperiment("C:\\DSSAT46\\Maize\\UFGA8201.MZX");
        }catch(IOException e){
            System.err.println("Cannot read experiment file:");
            System.err.println(e.getMessage());
            System.exit(1);
        }catch(FileParseException e){
            System.err.println("Cannot parse experiment file:");
            System.err.println(e.getMessage());
            System.exit(1);
        }

        // Create a new object to represent the irrigation scheme we want to try
        ResourceData irrigationData = new ResourceData();

        // Enter the irrigation data given to us into ResourceData object
        int totalIrrigation = 0;

        for(int i = 0; i < x.length; i++){
            // record the total irrigation
            totalIrrigation += x[i];
            
            // record the irrigation amount for this particular day
            irrigationData.addDatum(ResourceDatum.Substance.WATER,
            dates[i][0],dates[i][1],dates[i][2], x[i]);
        }

        exp.setIrrigationData(irrigationData);

        results[0] = totalIrrigation;

        try{
            // Try to save the experiment file
            exp.saveFile();
            // Run the experiment file you just created with DSSAT
            results[1] = exp.run().getYield();
        }catch(IOException e){
            System.err.println("Could not write to output file:");
            System.err.println(e.getMessage());
            System.exit(1);
        }catch(FailedRunException e){
            System.err.println("Run failed. See WARNING.OUT");
            System.err.println(e.getMessage());
            System.exit(1);
        }
        
        return results;

    }


    public static int[][] getRandomDates(int amount, int minYear, int minMonth, int minDayOfMonth,
        int maxYear, int maxMonth, int maxDayOfMonth){

        int[][] result = new int[amount][3];

        long minDate = (new GregorianCalendar(minYear, minMonth, minDayOfMonth)).getTimeInMillis();
        long maxDate = (new GregorianCalendar(maxYear, maxMonth, maxDayOfMonth)).getTimeInMillis();

        GregorianCalendar[] dates = new GregorianCalendar[amount];

        for(int i = 0; i < dates.length; i++) {
            do {
                long randomDate = ThreadLocalRandom.current().nextLong(minDate, maxDate + 1);
                dates[i] = new GregorianCalendar();
                dates[i].setTime(new Date(randomDate));
            }while(! isUnique(dates[i], Arrays.copyOfRange(dates,0,i)));

            result[i][0] = dates[i].get(Calendar.YEAR);
            result[i][1] = dates[i].get(Calendar.MONTH);
            result[i][2] = dates[i].get(Calendar.DAY_OF_MONTH);

        }

        return result;

    }

    private static boolean isUnique(GregorianCalendar date, GregorianCalendar[] dates){
        boolean result = true;
        for(int i = 0; i < dates.length; i++){
            if(date.get(Calendar.YEAR) == dates[i].get(Calendar.YEAR) &&
                date.get(Calendar.DAY_OF_YEAR) == dates[i].get(Calendar.DAY_OF_YEAR) )
                result = false;
        }
        return result;
    }



}

