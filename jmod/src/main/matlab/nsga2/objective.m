function out = objective(variables)
    javaaddpath(fullfile('Objective.class'),'-end');

    maxVal = 50;
    
    scaledVars = variables * maxVal;
    
    obj = Objective;

    numberOfDates = 5;

    dates = javaMethod('getRandomDates',obj, numberOfDates, 1982,3,1,1982,6,15);
    dates = [1982 3 18; 1982 4 25; 1982 5 4; 1982 5 28; 1982 6 9];

    % Transfer matlab int32 matrix into a java Integer array 
    javaDates = javaArray('java.lang.Integer', numberOfDates, 3);
    for m = 1:numberOfDates
        for n = 1:3
            javaDates(m,n) = java.lang.Integer(dates(m,n));
        end 
    end 

    irrigation = javaArray('java.lang.Integer', numberOfDates);
    
    
    for m = 1:size(scaledVars,1)
    
        for n = 1:length(scaledVars(m,:))
            irrigation(n) = java.lang.Integer(scaledVars(m,n));
        end

        objectives =  javaMethod('obj', obj, javaDates, irrigation);
                
        if(m == 1)
            result = [objectives(1), -objectives(2)] ;
        else
            result = [result; [objectives(1), -objectives(2)] ];
        end
            
%         fprintf('Variables: [');
%         fprintf('%f,', scaledVars(m,:));
%         fprintf(']\n');
%   
%         fprintf('Objectives: [%f, %f', objectives(1), -objectives(2) );
%         fprintf(']\n');
    end

    out = result;
    
end

