
rng default
options = optimoptions(@gamultiobj,'MaxGenerations', 200,'display','iter', 'InitialPopulationRange',[0.25;0.75],'PlotFcn',@gaplotpareto);
x = gamultiobj(@objective,5,[],[],[],[],[0,0,0,0,0],[1,1,1,1,1], options);
%plot(x(:,1),x(:,2),'ko');

finalResults = objective([x(:,1) x(:,2) x(:,3) x(:,4) x(:,5)]);

plot(finalResults(:,1),-finalResults(:,2),'r*');
ylabel('Yield (kg/ha)')
xlabel('Total irrigation (mm)')
title('Pareto Front')
legend('Pareto front')


