import urllib
import sys
import re
from urllib.request import urlopen
import urllib.request

month = "201809"
day_min = 1
day_max = 31
base_url = "https://nomads.ncdc.noaa.gov/data/ndfd/%s/%s/%s"
zone = "KWNC"
rain = "YYFZ94"


# TODO make another loop to include different months

# For each day in our date range...
for day in range(day_min, day_max + 1):

    # Download the month web page
    month_url = base_url % (month, str(month) + str(day).zfill(2), "")
    response = urllib.request.urlopen(month_url)
    html = str(response.read())

    # Scrape the date urls from the web page for the zone that we want
    file_names = re.findall(r"href=\"%s_%s[^\"]*\"" % (rain,zone), html)

    # TODO make another for loop to include your different code (tmax, tmin)!

    # Download each date
    for r in file_names:
        file_name = r[6:-1]            
        full_url = base_url % (month, str(month) + str(day).zfill(2), file_name)
        print("Downloading %s..." % full_url) 
        urllib.request.urlretrieve(full_url, file_name) 
        print("Done.") 




