#!/usr/bin/env python3
"""
# GRIDGEN: Function to convert wgrib2 text out file into ASCII grid file
# @author: J. Sebastian Hernandez (08/13/2019)
"""
import numpy as np

def gridgen(X0,Y0,CZ,ND,filename,fileout):
    
    fileID = open(filename,'r')
    nxny = np.loadtxt(fileID,delimiter=' ', max_rows = 1).astype(int)
    data = np.loadtxt(fileID,delimiter=' ')
    fileID.close()
    
    # grid construction
    data[data==ND] = 999.999
    grid = np.zeros((nxny[1],nxny[0]))
    c = 0
    for i in range(0,nxny[1]):
        for j in range(0,nxny[0]):
            grid[nxny[1]-(i+1),j] = data[c]
            c = c+1
    
    # ASCII file generation
    fileID = open(fileout,'w')
    fileID.write('NCOLS %2d\n'% nxny[0]);
    fileID.write('NROWS %2d\n'% nxny[1]);
    fileID.write('XLLCENTER %6.3f\n' % X0);
    fileID.write('YLLCENTER %6.3f\n'% Y0);
    fileID.write('CELLSIZE %4.3f\n'% CZ);
    fileID.write('NODATA_VALUE %6.3f\n' % 999.999);
    
    for i in range(0,nxny[1]):
        s = np.array2string(grid[i,], formatter={'float_kind':lambda x: "%6.3f" % x}, max_line_width = np.inf).replace('[','').replace(']','')
        if i == nxny[1]-1:
            fileID.write('%s'% s)
        else:
            fileID.write('%s\n'% s)
    
    fileID.close()