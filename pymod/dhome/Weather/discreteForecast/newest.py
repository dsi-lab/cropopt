#!/usr/bin/env python3
"""
# GRIB-2 files conversion to ASCII format
# @author: J. Sebastian Hernandez (08/22/2019)

# NOTE: Don't forget >> chmod +x data_extract.py to make script executable
# Type ./data_extract to run the code in bash (from the containing folder)
"""
# Libraries
import os
import subprocess
import numpy as np
import pandas as pd
from datetime import datetime as dt
import gridgen as gd

### INPUT SETTINGS ###
# Required parameters!!!
inputDIR = './2018_Forecast/'
outputDIR_TS = './All_Data'
opt_exp = 0; # export GRIB files to .asc files: 0-No; 1-Yes
opt_prob = 1; # 0- regular variables; 1-probabilities
X = -86.171 # Longitude of point of interest
Y = 42.0249 # Latitude of point of interest

# Input parameters for building .asc file (must be obtained from GRIB file) [required if using opt_exp]
outputDIR = './ASCII_Files/'
X0 = 584123.9329;    # X coordinate of the raster origin
Y0 = 1559717.7653;   # Y coordinate of the raster origin
CZ = 5079.406;       # Cellsize
ND = 9.999e20;       # No data value

### Main CODE starts here ###
if opt_exp == 1:
    if not os.path.exists(outputDIR): # create output directory if it doesn't exist 
        os.makedirs(outputDIR)

dirList = os.listdir(inputDIR)
c = 0
for folder in dirList:
    wkDIR = inputDIR+folder
    varList = os.listdir(wkDIR)
    if opt_prob == 0:
        varList.remove('Probability')
    else:
        varList = os.listdir(wkDIR+'/Probability')
        wkDIR = wkDIR+'/Probability'
        
    for var in varList:
        gribFiles = os.listdir(wkDIR+'/'+var)
        gribFiles = [item for item in gribFiles if not item.endswith('.py')]
        for gribFile in gribFiles:
            inputDIRFile = os.path.abspath(wkDIR+'/'+var+'/'+gribFile)            
            ps = subprocess.check_output(['wgrib2','-s',inputDIRFile]).decode("utf-8")
            ps = ps.split('\n')[:-1]
            fc = subprocess.check_output(['wgrib2','-start_ft','-end_ft',inputDIRFile]).decode("utf-8")
            fc = fc.split('\n')[:-1]
            for rec in ps:
                # extract time and forecast range
                temp = rec.split(':')
                ind = [ii for ii in range(len(ps)) if ps[ii] == rec]
                temp2 = fc[ind[0]].split(':')
                time = temp[2][2:]
                fcTimeStart = temp2[2][9:]
                fcTimeEnd = temp2[3][7:]                     
                # query values from input coordinates
                wg1 = subprocess.Popen(['wgrib2','-s',inputDIRFile],stdout=subprocess.PIPE)
                wg2 = subprocess.Popen(['grep',rec], stdin=wg1.stdout,stdout=subprocess.PIPE)                
                output = subprocess.check_output(['wgrib2','-i',inputDIRFile,'-lon',str(X),str(Y)],stdin=wg2.stdout).decode("utf-8")
                val = float(output.split('val=')[1].split('\n')[0])
                # build table
                record = np.array([var,dt.strptime(time,'%Y%m%d%H'),dt.strptime(fcTimeStart,'%Y%m%d%H'),dt.strptime(fcTimeEnd,'%Y%m%d%H'),val])
                if c == 0:
                    res = record
                else:
                    res = np.vstack((res,record))    
                c = c + 1
                
                if opt_exp == 1:
                    # output file name
                    outFile1 = var +'_' + time + '_' + fcTimeStart + '_' + fcTimeEnd + '.txt'
                    outFile2 = var +'_' + time + '_' + fcTimeStart + '_' + fcTimeEnd + '.asc'
                    outputDIRFile1 = os.path.abspath(outputDIR+outFile1)
                    outputDIRFile2 = os.path.abspath(outputDIR+outFile2)
                    # export GRIB file to txt and save it in output folder
                    wg1 = subprocess.Popen(('wgrib2','-s',inputDIRFile),stdout=subprocess.PIPE)
                    wg2 = subprocess.Popen(('grep',rec), stdin=wg1.stdout,stdout=subprocess.PIPE)
                    output = subprocess.check_output(('wgrib2','-i',inputDIRFile,'-text',outputDIRFile1),stdin=wg2.stdout)
                    # convert txt file to ESRI .asc file
                    filename = outputDIRFile1 # directory to input .txt file
                    fileout = outputDIRFile2  # directory to output .asc file
                    gd.gridgen(X0,Y0,CZ,ND,filename,fileout) # running function for creating .asc file
                    os.remove(outputDIRFile1)
                
            print('GRIB file ' + gribFile + ' successfully read...')
            
# wrapping-up...
res = pd.DataFrame(data=res, columns = ['Variable','Date','Frcst_Start','Frcst_End','Value'])
res = res.sort_values(['Variable','Date','Frcst_End'])
# save pandas data frame for future use (see NDFD_TS.py script!)
res.to_pickle(outputDIR_TS+'_opt_'+ str(opt_prob))
if opt_prob == 1:
    res.to_csv('Prob_values.txt', sep='\t', encoding = 'utf-8', index = False)