#!/usr/bin/env python3
""" 
# Utility to complete time series based on a reference time series
# @author: J. Sebastian Hernandez (08/23/2019)
"""

import numpy as np
from scipy import stats, interp

def comp_ts(ts1,ts2):    
    
    # removing NaN for regression analysis
    X=ts1[np.logical_not(np.isnan(ts2))]
    Y=ts2[np.logical_not(np.isnan(ts2))]
    
    Y=Y[np.logical_not(np.isnan(X))]
    X=X[np.logical_not(np.isnan(X))]
    
    lm = stats.linregress(X,Y)
    
    # completing time series using linear regression results
    ts2[np.isnan(ts2)] = lm[1]+lm[0]*ts1[np.isnan(ts2)]
    ts1[np.isnan(ts1)] = (ts2[np.isnan(ts1)]-lm[1])/lm[0]
    
    # completing time series using linear interpolation
    ts1 = linint0(ts1)
    ts2 = linint0(ts2)
    
    out = list([ts1,ts2])
    
    return out
    
def linint0(ts):
    # completing time series using linear interpolation
    x = np.linspace(0,len(ts)-1,num=len(ts))
    x1 = x[np.logical_not(np.isnan(ts))]
    y1 = ts[np.logical_not(np.isnan(ts))]
    y = interp(x,x1,y1)    
    return y