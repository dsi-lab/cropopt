#!/usr/bin/env python3
"""
# Obtain NDFD time series for variables and period of interest
# @author: J. Sebastian Hernandez (08/22/2019)
# NOTE: you need to run "./data_extract.py" first!!!
"""
# Libraries
import pandas as pd
import numpy as np
import comp_ts as cmp

### INPUT SETTINGS ###
inputFile = './All_Data_opt_0'

df = pd.read_pickle(inputFile)
df.Value = df.Value.astype(float)

# prepare data frame
df.loc[(df['Variable'] != 'Rain') & (df['Frcst_lower'] <  12) & (df['Frcst_lower'] >  5), 'Frcst_lower'] = 12
df.loc[(df['Variable'] != 'Rain') & (df['Frcst_lower'] <  12) & (df['Frcst_lower'] >  5), 'Frcst_upper'] = 24
df.loc[(df['Variable'] != 'Rain') & (df['Frcst_lower'] <= 18) & (df['Frcst_lower'] > 12), 'Frcst_lower'] = 12
df.loc[(df['Variable'] != 'Rain') & (df['Frcst_lower'] <= 18) & (df['Frcst_lower'] > 12), 'Frcst_upper'] = 24
df.loc[(df['Variable'] != 'Rain') & (df['Frcst_lower'] <  24) & (df['Frcst_lower'] > 18), 'Frcst_lower'] = 24
df.loc[(df['Variable'] != 'Rain') & (df['Frcst_lower'] <  24) & (df['Frcst_lower'] > 18), 'Frcst_upper'] = 36
  
## the long (and very unefficient) way... [done for 'validation' purposes]
#c = 0
#for var in df.Variable.unique():
#    for i in df.Date.unique():
#        for j in df.Frcst_lower.unique():
#            temp = df.loc[(df.Variable == var) & (df.Date == i) & (df.Frcst_lower == j)]
#            if not temp.empty:
#                avg = temp.Value.mean()
#                record = np.array([var,i,j,temp.iloc[0].Frcst_upper,avg])
#                if c == 0:
#                    res = record
#                else:
#                    res = np.vstack((res,record))
#                c = c + 1
#
#res = pd.DataFrame(data=res, columns = ['Variable','Date','Frcst_lower','Frcst_upper','Value'])
#res = res.sort_values(['Variable','Date','Frcst_lower'])
#
#ts0 = {}
#for var in df.Variable.unique():
#    df_temp = res.loc[res.Variable == var]
#    ts0[var] = df_temp.pivot(index='Date',columns = 'Frcst_lower', values = 'Value')

# efficient way
ts = {}
for var in df.Variable.unique():
    df_temp = df.loc[df.Variable == var]
    ts[var] = df_temp.pivot_table(index='Date',columns = 'Frcst_lower', values = 'Value', aggfunc = np.mean)

# write out precipitation data
ts_out ={}
rm_col = np.linspace(1,16,16, dtype=int)*6
ts_out['Rain'] = ts['Rain'][rm_col]

#  write out completed temperature data
T_tmin = np.linspace(1,8,8)*24-12
T_tmax = np.linspace(1,8,8)*24

for i in range(0,len(T_tmin)):
    ts1 = np.array(ts['Tmax'][T_tmax[i]])
    ts2 = np.array(ts['Tmin'][T_tmin[i]])
    new_ts = cmp.comp_ts(ts1,ts2)
    if (i==0):
        ts_out['Tmax'] = new_ts[0].reshape(-1,1)-273.15
        ts_out['Tmin'] = new_ts[1].reshape(-1,1)-273.15
    else:
        ts_out['Tmax'] = np.hstack((ts_out['Tmax'], new_ts[0].reshape(-1,1)-273.15))
        ts_out['Tmin'] = np.hstack((ts_out['Tmin'], new_ts[1].reshape(-1,1)-273.15))

ts_out['Tmin'] = pd.DataFrame(ts_out['Tmin'], index = ts['Tmin'].index, columns = T_tmin.astype(int))
ts_out['Tmax'] = pd.DataFrame(ts_out['Tmax'], index = ts['Tmax'].index, columns = T_tmax.astype(int))