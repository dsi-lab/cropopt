
function labelFig(figTitle, xLabel, yLabel, zLabel)

    xDim = 1100;
    yDim = 900;

    if ~exist('figTitle','var')
        error('Usage labelFig(TITLE [, XLABEL [, YLABEL[, ZLABEL]]])');
    end
    titleFont = 30;
    labelFont = 30;

    title(figTitle, 'FontSize', titleFont);

    if exist('xLabel','var')
        xlabel(xLabel, 'FontSize', labelFont);
    end
    
    if exist('yLabel','var')
        ylabel(yLabel, 'FontSize', labelFont);
    end
    
    if exist('zLabel','var')
        zlabel(zLabel, 'FontSize', labelFont);
    end
    
    hFig = figure(1);
    set(hFig, 'Position', [0 0 xDim yDim]);

end
