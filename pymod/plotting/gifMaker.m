function gifMaker(xData, yData, zData, wData, out)
        
    if ~exist('xData') || ~exist('yData') || ~exist('zData') || ~exist('out')
        error("Usage: gifMaker(xData, yData, zData, out)");
    end

    xMin = min(xData);
    xMax = max(xData);
    yMin = min(yData);
    yMax = max(yData);
    zMin = min(zData);
    zMax = max(zData);
    
    h = figure; 
    
    colormap("jet");
    scatter3(xData, yData, zData, 90, wData, "filled");

    colorbar;

    title = "Pareto Optimal set";
    xLabel = "Mean Yield (kg/ha)";
    yLabel = "Mean Leaching (kg/ha)";
    zLabel = "Mean Water Use (mm)";
    
    set(gca,'FontSize',35)

    labelFig(title, xLabel, yLabel, zLabel);
    
    view(125,25)
    grid on

    for n = (1:720)
        labelFig(title, xLabel, yLabel, zLabel);

        axis([xMin xMax yMin yMax zMin zMax]);
        drawnow
        frame = getframe(h); 
        camorbit(0.5,0,'data',[0 0 1]);
        im = frame2im(frame); 

        [imind,cm] = rgb2ind(im,128); 

        if n == 1 
           imwrite(imind,cm,out,'gif', 'Loopcount',Inf,'DelayTime',1/10); 
        else 
           imwrite(imind,cm,out,'gif', 'WriteMode','append','DelayTime',1/10); 
        end 

    end 
    
    
end
