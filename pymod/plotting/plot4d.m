file = '/Volumes/Gilgamesh/kroppian/weatherUncertainty/year_2018_2022-07-15_10-23-16/run0000_gen0200_obj.csv';

M = readmatrix(file);

colormap("jet");

scatter3(-M(:,2), M(:,3), M(:,4), 90, 1-M(:,1), "filled");

colorbar    

xlabel("Average yield");
ylabel("Average leaching");
zlabel("Average water");
