import sys
import pandas as pd
from dssatmod.dssat4dum import Dssat4Dum
from cropopt import CropOpt
import numpy as np


# Example command: 
# python python validation/validatePO.py 1

if len(sys.argv) != 2:
    print("Usage: %s SOL_NO" % sys.argv[0])
    sys.exit(1)

run_no = int(sys.argv[1])

PO_PRACTICES = "/Volumes/Gilgamesh/kroppian/weatherUncertainty/year_2018_2022-07-15_10-23-16/run0000_gen0200_var.csv"
OUTPUT_PATH = "/mnt/nas/kroppian/agovization_results/validation/"

# Gather weather data
wth_file_path = "dhome/Weather/CASSREPR.WTH"
gdd_tab_path = "management_dates.csv"

wth_tab = pd.read_fwf(wth_file_path, skiprows=4)
gdd_tab = pd.read_csv(gdd_tab_path)
practices = pd.read_csv(PO_PRACTICES, header=None)

# DSSAT parameters
home_dir = "/Users/iankropp/"
#home_dir = "/home/ian/"

dssat_home = "%s/Projects/cropopt/pymod/dhome" % home_dir
dssat_exe = "%s/Projects/cropopt/pymod/dhome/dscsm047-linux" % home_dir
#dssat_exe = "%s/Projects/cropopt/pymod/dhome/dscsm047-macos" % home_dir
dssat_inp = "%s/Projects/cropopt/pymod/dhome/DSSAT47.INP" % home_dir
output_dir = "%s/Projects/cropopt/pymod/output/" % home_dir

tmp_dir = "/tmp/"

threads = 8

# Initialize DSSAT runner
dssat = Dssat4Dum(dssat_home, dssat_inp, dssat_exe, tmp_dir)

# inputs for DSSAT
schedules_common_pract = []
schedules_irr_only_pract = []
schedules_nitro_only_pract = []
schedules_all_recs = []

YEAR = 2018

if YEAR % 4 == 0:
    PLANT_DATE = 136
else:
    PLANT_DATE = 135


# Date ranges

date_ranges = [[2018189, 2018254, 0, 0, 30, 0], [2018181, 2018231, 1, 50, 0, 0]]
date_ranges = np.array(date_ranges)


## Run set up 

x_rounded = np.around(practices)

x_rounded = x_rounded.to_numpy()

updates = []

updates.append({ 'pdate': YEAR*1e3 + PLANT_DATE, 'sdate': YEAR*1e3 + PLANT_DATE, 'icdat': YEAR*1e3 + PLANT_DATE })

applications = CropOpt.build_applications(x_rounded, date_ranges)

print(applications[run_no, :, :])






